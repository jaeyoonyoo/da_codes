#!/bin/sh

export DATA_DIR=/home/yjy765/BigData_Result

mkdir -p $DATA_DIR/svhn

echo "$DATA_DIR/" > data_dir.txt

wget -O $DATA_DIR/svhn/train_32x32.mat http://ufldl.stanford.edu/housenumbers/train_32x32.mat
wget -O $DATA_DIR/svhn/test_32x32.mat http://ufldl.stanford.edu/housenumbers/test_32x32.mat
