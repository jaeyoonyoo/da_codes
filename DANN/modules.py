import tensorflow as tf
import sys
sys.path.append('../utils')
import op

class generator(object):
    def __init__(self, source_data, target_data,args):
        #self.noise = tf.random_uniform(shape=[args.batch_size,args.noise_dim],minval=-1,maxval=1,dtype=tf.float32,name='random_noise')
        pass
    def mnist2mnistm(self,x,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnist2mnistm'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.conv2d(x, out_channel = 32, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h1 = op._max_pool(h1)
                layer_idx += 1
                h2 = op.conv2d(h1, out_channel = 48, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h2 = op._max_pool(h2)
            return tf.reshape(h2,[-1,7*7*48])
    def mnistm2mnist(self,x,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnistm2mnist'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.conv2d(x, out_channel = 32, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h1 = op._max_pool(h1)
                layer_idx += 1
                h2 = op.conv2d(h1, out_channel = 48, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h2 = op._max_pool(h2)
            return tf.reshape(h2,[-1,7*7*48])

    def mnist2svhn(self,x,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnist2svhn'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.conv2d(x, out_channel=64, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h1 = op._max_pool(h1,[1,3,3,1])
                layer_idx += 1
                h2 = op.conv2d(h1, out_channel=64, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h2 = op._max_pool(h2,[1,3,3,1])
                layer_idx += 1
                h3 = op.conv2d(h2, out_channel=128, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
            return tf.reshape(h3,[-1,8*8*128])
    def svhn2mnist(self,x,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('svhn2mnist'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.conv2d(x, out_channel=64, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h1 = op._max_pool(h1,[1,3,3,1])
                layer_idx += 1
                h2 = op.conv2d(h1, out_channel=64, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
                h2 = op._max_pool(h2,[1,3,3,1])
                layer_idx += 1
                h3 = op.conv2d(h2, out_channel=128, filter_size=5, stride=1, name='conv%d'%layer_idx, activation=tf.nn.relu, normalization=False)
            return tf.reshape(h3,[-1,8*8*128])
       
        
class discriminator(object):
    def __init__(self, source_data, target_data,args):
        pass
    def mnist2mnistm(self,features, name, reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnist2mnistm'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,100,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,2,name='fc%d'%layer_idx,dropout=False,activation=False)
                logit = tf.nn.softmax(h2)
            return logit
    def mnistm2mnist(self,features, name, reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnistm2mnist'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,100,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,2,name='fc%d'%layer_idx,dropout=False,activation=False)
                logit = tf.nn.softmax(h2)
            return logit
    def mnist2svhn(self,features,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnist2svhn'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,1024,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,1024,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h3 = op.fc(h2,2,name='fc%d'%layer_idx,dropout=False,activation=False)
                logit = tf.nn.softmax(h3)
            return logit
    def svhn2mnist(self,features,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('svhn2mnist'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,1024,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,1024,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h3 = op.fc(h2,2,name='fc%d'%layer_idx,dropout=False,activation=False)
                logit = tf.nn.softmax(h3)
            return logit

class task(object):
    def __init__(self, source_data, target_data,args):
        pass
    def mnist2mnistm(self,features,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnist2mnistm'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,100,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,100,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h3 = op.fc(h2,10,name='fc%d'%layer_idx,dropout=False,activation=False)
                #logit = tf.nn.softmax(h3)
                logit = h3
            return logit
    def mnistm2mnist(self,features,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnistm2mnist'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,100,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,100,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h3 = op.fc(h2,10,name='fc%d'%layer_idx,dropout=False,activation=False)
                #logit = tf.nn.softmax(h3)
                logit = h3
            return logit
    def mnist2svhn(self, features,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('mnist2svhn'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,3072,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,2048,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h3 = op.fc(h2,10,name='fc%d'%layer_idx,dropout=False,activation=False)
                logit=h3
            return logit
    def svhn2mnist(self, features,name,reuse=False):
        with tf.variable_scope(name):
            with tf.variable_scope('svhn2mnist'):
                if reuse:
                    tf.get_variable_scope().reuse_variables()
                layer_idx = 1
                h1 = op.fc(features,3072,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h2 = op.fc(h1,2048,name='fc%d'%layer_idx,dropout=False)
                layer_idx +=1
                h3 = op.fc(h2,10,name='fc%d'%layer_idx,dropout=False,activation=False)
                logit=h3
            return logit
