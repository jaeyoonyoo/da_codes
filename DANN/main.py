import tensorflow as tf
import os
import argparse
from train import train

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('--dloss_coeff',type=float,default=1.0)
  parser.add_argument('--closs_coeff',type=float,default=1.0)
  parser.add_argument('--batch_size',type=int, default=64)
  parser.add_argument('--noise_dim',type=int, default=128)
  parser.add_argument('--dataset',type=int,default=0) ## 0 : mnist -> mnistm, 1: mnistm -> mnist 2: mnist -> svhn 3: svhn -> mnist
  args = parser.parse_args()
  if args.dataset>3:
      print('dataset should not be larger than 3')
      raise NotImplementedError
  config = tf.ConfigProto()
  config.gpu_options.allow_growth = True

  with tf.Session(config=config) as sess:
      train(sess,args)

if __name__ == "__main__":
    main()
