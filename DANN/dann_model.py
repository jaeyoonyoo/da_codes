import tensorflow as tf
import modules
import losses

class model():
    def __init__(self, source_data, target_data, args):
        self.source_data   = source_data
        self.target_data   = target_data
        self.generator     = modules.generator(source_data, target_data, args)
        self.discriminator = modules.discriminator(source_data, target_data, args)
        self.task          = modules.task(source_data, target_data, args) 
        

    def __call__(self, source, target):
        with tf.name_scope('generator'):
            try:
                generator_func = getattr(self.generator, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no generator for source : %s target :%s' % (self.source_data, self.target_data))
            self.source_feature = generator_func(source, name='feature_generator', reuse=False)
            self.target_feature = generator_func(target, name='feature_generator', reuse=True)
        with tf.name_scope('discriminator'):
            try:
                disc_func = getattr(self.discriminator, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no discriminator for source : %s target : %s' % (self.source_data, self.target_data))
            self.disc_source = disc_func(self.source_feature, name='domain_disc', reuse=False)
            self.disc_target = disc_func(self.target_feature, name='domain_disc', reuse=True)
        with tf.name_scope('task'):
            try:
                task_func = getattr(self.task, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no task classifier for source : %s target : %s ' % (self.source_data, self.target_data))
            self.task_logit = task_func(self.source_feature, name='task_classifier')

    def create_objective(self, label):
        with tf.name_scope('adversarial'):
            self.g_loss, self.d_loss = losses.adversarial_loss(self.disc_source, self.disc_target)
        with tf.name_scope('classification'):
            self.classification_loss = losses.classification_loss(self.task_logit,label)
