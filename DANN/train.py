import tensorflow as tf
import numpy as np
from dann_model import model
import pickle
from tensorflow.examples.tutorials.mnist import input_data
import os
import sys
sys.path.append('../utils')
import op
from utils import *
from sklearn.manifold import TSNE
import get_data

def _get_trainable_vars(scope):
    is_trainable = lambda x : x in tf.trainable_variables()

    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope)
    var_list = list(filter(is_trainable, var_list))
    return var_list

def _get_optimizer(name, optimizer, loss, clip_norm=5.0):
    var_list = _get_trainable_vars(name)
    print(name)
    print('--------')
    print(var_list)
    print('========')
    grad, var = zip(*optimizer.compute_gradients(loss, var_list=var_list))
    gradients = [gradient if gradient is None else tf.clip_by_norm(gradient, clip_norm) for gradient in grad]
    optim = optimizer.apply_gradients(zip(gradients,var))
    #optim = optimizer.apply_gradients(zip(grad,var))
    return optim


def train(sess, args):
    dataset = [['mnist','mnistm'],['mnistm','mnist'],['mnist','svhn'],['svhn','mnist']]
    datasize = [[(28,28,3),(28,28,3)],[(28,28,3),(28,28,3)],[(32,32,3),(32,32,3)],[(32,32,3),(32,32,3)]]
    source_data = dataset[args.dataset][0]
    target_data = dataset[args.dataset][1]
    source_size = datasize[args.dataset][0]
    target_size = datasize[args.dataset][1]
    dann_model  = model(source_data, target_data, args)

    get_source = getattr(get_data, source_data)
    get_target = getattr(get_data, target_data)
    
    source_train, source_valid, source_test, source_mean, source_std, source_train_label, source_valid_label, source_test_label  = get_source(size=source_size)
    target_train, target_valid, target_test, target_mean, target_std, target_train_label, target_valid_label, target_test_label  = get_target(size=target_size)    

    print(target_test.shape)
    print(target_test_label.shape)

    #if source_train.shape[1:] != source_size:
    #    source_train =  resize_images(source_train, source_size)
    #    source_test  =  resize_images(source_test,  source_size)
    #if target_train.shape[1:] != target_size:
    #    target_train =  resize_images(target_train, target_size)
    #    target_test  =  resize_images(target_test,  target_size)


    source_batch = tf.placeholder(tf.float32, (None,)+source_size)
    source_label = tf.placeholder(tf.int8, (None,10))
    target_batch = tf.placeholder(tf.float32, (None,)+target_size)

    dann_model(source_batch, target_batch)
    dann_model.create_objective(source_label)

    generator_loss = args.dloss_coeff * dann_model.g_loss + args.closs_coeff * dann_model.classification_loss
    discriminator_loss = args.dloss_coeff * dann_model.d_loss
    classifier_loss= args.closs_coeff * dann_model.classification_loss

    #Eval
    domain_correct = tf.concat([tf.equal(tf.constant(np.zeros(args.batch_size),tf.int64), tf.argmax(dann_model.disc_source,1)) , tf.equal(tf.constant(np.ones(args.batch_size),tf.int64),tf.argmax(dann_model.disc_target,1))],0)
    print(domain_correct.get_shape())
    domain_acc     = tf.reduce_mean(tf.cast(domain_correct, tf.float32))
    task_correct   = tf.equal(tf.argmax(source_label,1), tf.argmax(dann_model.task_logit,1))
    task_acc       = tf.reduce_mean(tf.cast(task_correct, tf.float32))
    
    with tf.name_scope('optimizer'):
        learning_rate = tf.placeholder(tf.float32,[])
        supervised_op = tf.train.MomentumOptimizer(learning_rate, 0.9).minimize(classifier_loss)
        g_optimizer   = tf.train.MomentumOptimizer(learning_rate, 0.9)
        d_optimizer   = tf.train.MomentumOptimizer(learning_rate, 0.9)
        c_optimizer   = tf.train.MomentumOptimizer(learning_rate, 0.9)
        g_op          = _get_optimizer('feature_generator',g_optimizer,generator_loss)
        d_op          = _get_optimizer('domain_disc',d_optimizer,discriminator_loss)
        c_op          = _get_optimizer('task_classifier',c_optimizer,classifier_loss)


    saver = tf.train.Saver(max_to_keep=5)
    sess.run(tf.global_variables_initializer())

    target_train = np.float32(target_train)

    gen_source_batch = batch_generator([source_train, source_train_label], args.batch_size)
    gen_target_batch = batch_generator([target_train, target_train_label], args.batch_size)


    #import matplotlib.pyplot as plt
    #import scipy.misc
    #import time
    #s_im, s_lb = gen_source_batch.__next__()
    #t_im, t_lb = gen_target_batch.__next__()
    #for i in range(3):
    #    print(s_lb[i])
    #    scipy.misc.imshow(s_im[i])
    #time.sleep(10)
    #print(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
    #print('===============================')
    print(target_train.dtype)
    print(np.max(target_train))
    print(np.min(target_train))
    print(source_train.dtype)
    print(np.max(source_train))
    print(np.min(source_train))
    num_steps = 10000
    for idx in range(num_steps):
        p = float(idx) / num_steps
        l = 2. / (1. + np.exp(-10. * p)) - 1
        d_lr = 0.001 / (1. + 10 * p)**0.75
        g_lr = 0.002 / (1. + 10 * p)**0.75
        if True:
            for _ in range(3):
                s_im, s_lb = gen_source_batch.__next__()
                t_im, t_lb = gen_target_batch.__next__()
                a,b,_, dloss, d_acc = sess.run([dann_model.disc_source, dann_model.disc_target, d_op, discriminator_loss, domain_acc],
                                                feed_dict={source_batch:s_im, source_label:s_lb, target_batch:t_im, learning_rate:d_lr})
            for _ in range(2):                
                _,_, dloss, closs, d_acc, c_acc = sess.run([g_op,c_op, discriminator_loss, classifier_loss, domain_acc, task_acc],
                                                          feed_dict={source_batch:s_im, source_label:s_lb, target_batch:t_im, learning_rate:g_lr})
            if idx % 150 == 0:
                print('dloss : %.3f closs : %.3f d_acc : %.3f c_acc : %.3f' % (dloss, closs, d_acc, c_acc))
            if idx % 450 == 0:
                target_acc = sess.run(task_acc,
                                     feed_dict={source_batch: target_test[:int(target_test.shape[0]/2)], source_label: target_test_label[:int(target_test.shape[0]/2)]})
                print('c_target_acc : %.3f' % target_acc)

        else:
            _, closs, c_acc = sess.run([supervised_loss, classifier_loss, task_acc],
                                        feed_dict={source_batch:s_im, source_label:s_lb, learning_rate:lr})

    source_acc = sess.run(task_acc,
                          feed_dict={source_batch: source_test, source_label: source_test_label})
    target_acc = sess.run(task_acc,
                          feed_dict={source_batch: target_test[int(target_test.shape[0]/2):], source_label: target_test_label[int(target_test.shape[0]/2):]})
    test_domain_acc = sess.run(domain_acc,
                               feed_dict={source_batch: source_test[:args.batch_size,:,:,:], target_batch: target_test[:args.batch_size,:,:,:]})
    s_emb, t_emb = sess.run([dann_model.source_feature, dann_model.target_feature], feed_dict={source_batch: source_test[:200,:,:,:], target_batch: target_test[:200,:,:,:]})

    print('Source accuracy : %.4f' % source_acc)
    print('Target accuracy : %.4f' % target_acc)
    print('domain accuracy : %.4f' % test_domain_acc)

    tsne= TSNE(perplexity=30, n_components=2, init='pca', n_iter=3000)
    dann_tsne = tsne.fit_transform(np.concatenate([s_emb,t_emb],0))
    emb_lbs = np.concatenate([np.argmax(source_test_label[:200,:],1),np.argmax(target_test_label[:200,:],1)],0)
    emb_domain = np.concatenate([np.zeros(200),np.ones(200)])
    plot_embedding(dann_tsne, emb_lbs, emb_domain, 'Domain Adaptation_from%sto%s' % (source_data,target_data))


