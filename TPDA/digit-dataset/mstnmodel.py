"""
Derived from: https://github.com/kratzert/finetune_alexnet_with_tensorflow/
"""
import tensorflow as tf
import numpy as np
import math
import sys
from util import *
sys.path.append('optimizers')
#from AMSGrad import AMSGrad
#ensure the ys and yt are two dimension

small_network = True

eps_M = 1e-7

# supervised semantic loss proposed in saied et al. ICCV17
def supervised_semantic_loss(xs,xt,ys,yt):
    #K=int(ys.get_shape()[-1])
    #return tf.constant(0.0)
    K=10
    classloss=tf.constant(0.0)
    for i in range(1,K+1):
        xsi=tf.gather(xs,tf.where(tf.equal(ys,i)))
        xti=tf.gather(xt,tf.where(tf.equal(yt,i)))
        xsi_=tf.expand_dims(xsi,0)
        xti_=tf.expand_dims(xti,1)
        distances=0.5*tf.reduce_sum(tf.squared_difference(xsi_,xti_))
        classloss+=distances
    classloss/=10.0
    
    return 0.0001*classloss

#squared Euclidean loss for prototypes
def protoloss(sc,tc):
    return tf.reduce_mean((tf.square(sc-tc)))


class LeNetModel(object):

    def __init__(self, num_classes=1000, is_training=True,image_size=28,dropout_keep_prob=0.5, sigma=1, FLAGS=None):
        self.num_classes = num_classes
        self.dropout_keep_prob = dropout_keep_prob
        self.sigma = sigma
        self.default_image_size=image_size
        self.is_training=is_training
        self.num_channels=1
        self.mean=None
        self.bgr=False
        self.range=None
        self.featurelen=10
        self.LP_closed_form     = FLAGS.LP_closed_form
        self.LP_iter            = FLAGS.LP_iter
        self.metric_form        = FLAGS.metric_form
        self.source_recon_loss  = FLAGS.source_recon_loss
        self.distance_form      = FLAGS.distance_form
        self.LP_or_LS           = FLAGS.LP_or_LS
        self.batch_size         = FLAGS.batch_size

    def svhn2mnist(self, x, training=False):
        conv1 = conv(x, 5, 5, 64, 1, 1, padding='SAME',bn=True,name='conv1')
        pool1 = max_pool(conv1, 3,3,2,2, padding='SAME',name='pool1')       
        conv2 = conv(pool1, 5, 5, 64, 1, 1, padding='SAME',bn=True,name='conv2')
        pool2 = max_pool(conv2, 3, 3, 2, 2, padding='SAME', name='pool2')
        
        conv3 = conv(pool2, 5, 5, 128, 1, 1, padding='SAME', name='conv3')

        flattened = tf.reshape(conv3, [-1, 7*7*128])
        self.feature = flattened

        fc1 = fc(flattened, 6272, 3072, bn=False,name='fc1')
        fc2 = fc(fc1, 3072, 2048, bn=False, name='fc2')
        self.score = fc(fc2, 2048, 10, bn=False, name='fc3')
        self.output = tf.nn.softmax(self.score)
        return self.score

    def inference(self, x, training=False):
        # 1st Layer: Conv (w ReLu) -> Pool -> Lrn
        conv1 = conv(x, 5, 5, 20, 1, 1, padding='VALID',bn=True,name='conv1')
        pool1 = max_pool(conv1, 2, 2, 2, 2, padding='VALID',name='pool1')

        # 2nd Layer: Conv (w ReLu) -> Pool -> Lrn with 2 groups
        conv2 = conv(pool1, 5, 5, 50, 1, 1, padding='VALID',bn=True,name='conv2')
        pool2 = max_pool(conv2, 2, 2, 2, 2, padding='VALID', name ='pool2')


        # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
        flattened = tf.contrib.layers.flatten(pool2)
        self.flattened=flattened
        fc1 = fc(flattened, 800, 500, bn=False,name='fc1')
        fc2 = fc(fc1, 500, 10, relu=False,bn=False,name='fc2')
        self.fc1=fc1
        self.fc2=fc2
        self.score=fc2
        self.output=tf.nn.softmax(self.score)
        self.feature=fc2
        return self.score
    def wganloss(self,x,xt,batch_size,lam=10.0):
        with tf.variable_scope('reuse_inference') as scope:
            scope.reuse_variables()
            if not small_network:
                self.svhn2mnist(x,training=True)
            else:
                self.inference(x,training=True)
            source_fc6=self.fc6
            source_fc7=self.fc7
            source_fc8=self.fc8
            source_softmax=self.output
            source_output=outer(source_fc7,source_softmax)
            print ('SOURCE_OUTPUT: ',source_output.get_shape())
            scope.reuse_variables()
            if small_network:
                self.inference(xt,training=True)
            else:
                self.svhn2mnist(xt,training=True)
            target_fc6=self.fc6
            target_fc7=self.fc7
            target_fc8=self.fc8
            target_softmax=self.output
            target_output=outer(target_fc7,target_softmax)
            print ('TARGET_OUTPUT: ',target_output.get_shape())
        with tf.variable_scope('reuse') as scope:
            target_logits,_=D(target_fc8)
            scope.reuse_variables()
            source_logits,_=D(source_fc8)
            eps=tf.random_uniform([batch_size,1],minval=0.0,maxval=1.0)
            X_inter=eps*source_fc8+(1-eps)*target_fc8
            grad = tf.gradients(D(X_inter), [X_inter])[0]
            grad_norm = tf.sqrt(tf.reduce_sum((grad)**2, axis=1))
            grad_pen = lam * tf.reduce_mean((grad_norm - 1)**2)
            D_loss=tf.reduce_mean(target_logits)-tf.reduce_mean(source_logits)+grad_pen
            G_loss=tf.reduce_mean(source_logits)-tf.reduce_mean(target_logits)    
            self.G_loss=G_loss
            self.D_loss=D_loss
            self.D_loss=0.3*self.D_loss
            self.G_loss=0.3*self.G_loss
        return G_loss,D_loss

    def adloss(self,x,xt,y,lamb,L,yt=None):
        with tf.variable_scope('reuse_inference') as scope:
            scope.reuse_variables()
            if small_network:
                source_score = self.inference(x,training=True)
            else:
                source_score = self.svhn2mnist(x,training=True)
            source_feature=self.feature
            scope.reuse_variables()
            if small_network:
                self.inference(xt,training=True)
            else:
                self.svhn2mnist(xt,training=True)
            target_feature=self.feature
            target_pred=self.output
        with tf.variable_scope('reuse') as scope:
            source_logits,_=D(source_feature)
            scope.reuse_variables()
            target_logits,_=D(target_feature)

        self.target_pred=target_pred    
        self.source_feature=source_feature
        self.target_feature=target_feature
        self.concat_feature=tf.concat([source_feature,target_feature],0)    

        source_result=tf.argmax(y,1)
        target_result=tf.argmax(target_pred,1)
    
        #!!!!!!!!!!!use groudn truth yt to test !!!!!!!!!!!!!!!!!!!
        #target_result=tf.argmax(yt,1)

        #### GET UNNORMALIZED SIMILARITY MATRIX W ####
        #### GET DISTANCE MATRIX M FIRST AND THEN CONVERT IT THROUGH GAUSSIAN ####
        if self.distance_form == 'euclidean':
            s_fb1 = tf.expand_dims(tf.transpose(source_feature,[1,0]),2)
            s_f1b = tf.transpose(s_fb1,[0,2,1])
            t_fb1 = tf.expand_dims(tf.transpose(target_feature,[1,0]),2)
            t_f1b = tf.transpose(t_fb1,[0,2,1])
            ## SS ##
            M_SS = tf.square(s_fb1 - s_f1b) + eps_M
            #We should review the validity of 'eps_M'
            sigma_tile = tf.tile(tf.expand_dims(tf.expand_dims(self.sigma,1),2),[1,self.batch_size,self.batch_size])
            M_SS = tf.reduce_mean(M_SS*tf.square(sigma_tile),0)
            W_SS = tf.exp(-M_SS/2)
            W_SS = W_SS - tf.diag(tf.diag_part(W_SS))

            ## ST & TS ##
            M_ST = tf.square(s_fb1 - t_f1b)
            self.W_TS = M_ST
            M_ST = tf.reduce_mean(M_ST*tf.square(sigma_tile),0)
            W_ST = tf.exp(-M_ST/2)
            W_TS = tf.transpose(W_ST,[1,0])
            ## TT ##
            M_TT = tf.square(t_fb1 - t_f1b) + eps_M
            M_TT = tf.reduce_mean(M_TT*tf.square(sigma_tile),0)
            W_TT = tf.exp(-M_TT/2)
            W_TT = W_TT - tf.diag(tf.diag_part(W_TT))
            self.M_SS = M_SS
            self.M_ST = M_ST
            self.M_TT = M_TT
            self.W_TT = W_TT
            self.W_SS = W_SS
            self.W_TS = W_TS
        elif self.distance_form == 'L1':
            s_fb1 = tf.expand_dims(tf.transpose(source_feature,[1,0]),2)
            s_f1b = tf.transpose(s_fb1,[0,2,1])
            t_fb1 = tf.expand_dims(tf.transpose(target_feature,[1,0]),2)
            t_f1b = tf.transpose(t_fb1,[0,2,1])
            ## SS ##
            M_SS = tf.abs(s_fb1 - s_f1b)
            M_SS = tf.reduce_mean(M_SS,0)
            W_SS = tf.exp(-tf.square(M_SS)/2/self.sigma/self.sigma)
            W_SS = W_SS - tf.diag(tf.diag_part(W_SS))
            ## ST & TS ##
            M_ST = tf.abs(s_fb1 - t_f1b)
            M_ST = tf.reduce_mean(M_ST,0)
            W_ST = tf.exp(-tf.square(M_ST)/2/self.sigma/self.sigma)
            W_TS = tf.transpose(W_ST,[1,0])
            ## TT ##
            M_TT = tf.abs(t_fb1 - t_f1b)
            M_TT = tf.reduce_mean(M_TT,0)
            W_TT = tf.exp(-tf.square(M_TT)/2/self.sigma/self.sigma)
            W_TT = W_TT - tf.diag(tf.diag_part(W_TT))
        elif self.distance_form == 'correlation':
            pass
        elif self.distance_form == 'log_correlation':
            pass
        elif self.distance_form == 'inner_product':
            #### EXCEPTION ####
            #### GET SIMILARITY WITHOUT DISTANCE MATRIX ####
            s_fb1 = tf.expand_dims(tf.transpose(source_feature,[1,0]),2)
            s_f1b = tf.transpose(s_fb1,[0,2,1])
            t_fb1 = tf.expand_dims(tf.transpose(target_feature,[1,0]),2)
            t_f1b = tf.transpose(t_fb1,[0,2,1])
            ## SS ##
            W_SS = s_fb1 * s_f1b
            W_SS = tf.reduce_sum(W_SS,0)
            ## ST & TS ##
            W_ST = s_fb1 * t_f1b
            W_ST = tf.reduce_sum(W_ST,0)
            W_TS = tf.transpose(W_ST,[1,0])
            self.W_TS = W_TS
            ## TT ##
            W_TT = t_fb1 * t_f1b
            W_TT = tf.reduce_sum(W_TT,0)
            self.W_TT = W_TT
        else:
            raise NotImplementedError('1)euclidean 2)L1 3)correlation 4)log_correlation 5)inner_product')
                

        #### LABEL PROPAGATION ####
        if self.LP_closed_form:
            ###### TODO ######
            if self.LP_or_LS == 'LP':
                print('-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-')
                print(W_SS.get_shape())
                print(W_TT.get_shape())
                yt_hat = LP_closed([W_TS,W_TT],y)
                y_hat  = LP_closed([W_ST,W_SS],yt_hat)
            elif self.LP_or_LS == 'LS':
                yt_hat = LS_closed([W_SS,W_ST,W_TS,W_TT],y)
                y_hat  = LS_closed([W_TT,W_TS,W_ST,W_SS],yt_hat)
            else:
                raise NotImplementedError('LP_or_LS should be among 1) LP or 2) LS')
        else:
            ##### TODO #######
            ##### THINK HOW TO INITIALIZE yt_hat #####
            if self.LP_or_LS == 'LP':
                yt_hat = tf.ones_like(y) / 10.
                y_hat  = tf.ones_like(y) / 10.
                for _ in range(self.LP_iter):
                    yt_hat = LP_iter([W_TS,W_TT],y,yt_hat)
                for _ in range(self.LP_iter):
                    y_hat  = LP_iter([W_ST,W_SS],yt_hat,y_hat)
            elif self.LP_or_LS == 'LS':
                yt_hat = tf.ones_like(y) / 10.
                y_hat  = tf.ones_like(y) / 10.
                y0_ST  = tf.concat([y,yt_hat],0)
                y_LP   = y
                for _ in range(self.LP_iter):
                    y_LP, yt_hat = LS_iter([W_SS,W_ST,W_TS,W_TT],y_LP,yt_hat,y0_ST)
                y0_TS  = tf.concat([yt_hat,y_hat],0)
                yt_hat_TS = yt_hat
                for _ in range(self.LP_iter):
                    yt_hat_TS, y_hat  = LS_iter([W_TT,W_TS,W_ST,W_SS],yt_hat_TS,y_hat,y0_TS)
            else:
                raise NotImplementedError('LP_or_LS should be among 1) LP or 2) LS')
        self.y_hat = y_hat
        print(tf.reduce_sum(y-y_hat,1).get_shape())
        self.cycleloss = tf.reduce_mean(tf.reduce_sum(tf.abs(y-y_hat),1))  + tf.reduce_mean(tf.nn.relu(tf.ones(self.batch_size)-tf.reduce_sum(y_hat,1)-0.2))
        self.grad_SS = tf.gradients(self.cycleloss,self.W_SS)
        self.grad_TS = tf.gradients(self.cycleloss,self.W_TS)
        self.grad_TT = tf.gradients(self.cycleloss,self.W_TT)
        ###### TODO ######
        if self.metric_form == 'quadratic':
            self.metricloss = Quadratic(self.source_feature, L) / self.batch_size / 100
        elif self.metric_form == 'CE':
            self.metricloss = CE(source_score, y)
        else:
            raise NotImplementedError('1)quadratic 2)CE')


        tf.summary.scalar('pure_metricloss',self.metricloss)
        self.puremtrloss = self.metricloss

        ### f norm ###
        self.metricloss = self.metricloss + lamb * tf.reduce_mean(tf.reduce_mean(source_feature * source_feature + target_feature * target_feature,1)) / 30.0
        #self.metricloss = self.metricloss + bond_energy(tf.reduce_mean(source_feature * source_feature), k1=10, k2=25) + bond_energy(tf.reduce_mean(target_feature * target_feature), k1=10, k2=25)
        
        if self.source_recon_loss:
            self.cycleloss += Source_LP(y, W_SS)
    
        tf.summary.scalar('metricloss',self.metricloss)
        tf.summary.scalar('cycleloss',self.cycleloss)

        self.Entropyloss=tf.constant(0.)    
    
        #!!!!!!!!!!!!compare with individual sample alignment with our centroid alignment method!!!!!!!!!
        #self.Semanticloss=supervised_semantic_loss(source_feature,target_feature,source_result,target_result)

        D_real_loss=tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=target_logits,labels=tf.ones_like(target_logits)))
        D_fake_loss=tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=source_logits,labels=tf.zeros_like(source_logits)))
        self.D_loss=D_real_loss+D_fake_loss
    
        self.G_loss=-self.D_loss
        tf.summary.scalar('JSD',self.G_loss/2+math.log(2))
    
        #------------- Domain Adversarial Loss is scaled by 0.1 following RevGrad--------------------------
        #self.G_loss=0.1*self.G_loss
        #self.D_loss=0.1*self.D_loss
        return self.G_loss,self.D_loss, y_hat, yt_hat

    def loss(self, batch_x, batch_y=None):
        with tf.variable_scope('reuse_inference') as scope:
            if small_network:
                y_predict = self.inference(batch_x, training=True)
            else:
                y_predict = self.svhn2mnist(batch_x, training=True)
            self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_predict, labels=batch_y))
        return self.loss

    def adoptimize(self,learning_rate,train_layers=[]):
        var_list=[v for v in tf.trainable_variables() if 'D' in v.name]
        D_weights=[v for v in var_list if 'weights' in v.name]
        D_biases=[v for v in var_list if 'biases' in v.name]
        print ('=================Discriminator_weights=====================')
        print (D_weights)
        print ('=================Discriminator_biases=====================')
        print (D_biases)
    
        self.Dregloss=5e-4*tf.reduce_mean([tf.nn.l2_loss(v) for v in var_list if 'weights' in v.name])
        D_op1 = tf.train.MomentumOptimizer(learning_rate,0.9).minimize(self.D_loss+self.Dregloss, var_list=D_weights)
        D_op2 = tf.train.MomentumOptimizer(learning_rate*2.0,0.9).minimize(self.D_loss+self.Dregloss, var_list=D_biases)
        D_op=tf.group(D_op1,D_op2)
        return D_op


    def optimize(self, learning_rate, train_layers,global_step,tf_grad_weight):
        print ('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print (train_layers)
        var_list=[v for v in tf.trainable_variables() if v.name.split('/')[1] in ['conv1','conv2','conv3','fc1','fc2','fc3','theta']]
        self.Gregloss=5e-4*tf.reduce_mean([tf.nn.l2_loss(x) for x in var_list if 'weights' in x.name])
    
        new_weights=[v for v in var_list if 'weights' in v.name or 'gamma' in v.name]
        new_biases=[v for v in var_list if 'biases' in v.name or 'beta' in v.name]

        for i in range(len(new_weights)):
            if new_weights[i].name.split('/')[1] == 'theta':
                sig_idx = i
        print ('==============new_weights=======================')
        print (new_weights)
        print ('==============new_biases=======================')
        print (new_biases)

        #self.F_loss= self.Gregloss+global_step*self.cycleloss*100 + global_step*self.G_loss  + global_step * self.metricloss
        self.F_loss= self.Gregloss + self.G_loss  + self.metricloss


        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        print ('+++++++++++++++ batch norm update ops +++++++++++++++++')
        print (update_ops)
        with tf.control_dependencies(update_ops):
            op3 = tf.train.MomentumOptimizer(learning_rate*1.0,0.9)
            op4 = tf.train.MomentumOptimizer(learning_rate*1.0,0.9)
            wo_cyc_grad3 = tf.gradients(self.F_loss,new_weights)
            wo_cyc_grad4 = tf.gradients(self.F_loss,new_biases)
            cyc_sf_grad  = tf.gradients(self.cycleloss * global_step * 10, self.source_feature)
            cyc_tf_grad  = tf.gradients(self.cycleloss * global_step * 10, self.target_feature)
            sf_grad3     = tf.gradients(self.source_feature, new_weights, cyc_sf_grad)
            sf_grad4     = tf.gradients(self.source_feature, new_biases, cyc_sf_grad)
            cyc_sigma_grad = tf.gradients(self.cycleloss * global_step * 10, new_weights[sig_idx])

            weighted_grad = cyc_tf_grad[0] * tf_grad_weight
            print(weighted_grad.get_shape())
            print(self.target_feature.get_shape())
            tf_grad3     = tf.gradients(self.target_feature, new_weights, weighted_grad)
            tf_grad4     = tf.gradients(cyc_tf_grad, new_biases, weighted_grad)
            grad3 = []
            grad4 = []
            for i in range(len(new_weights)):
                if sf_grad3[i] == None:
                    if i == sig_idx:
                        grad3.append(wo_cyc_grad3[i] + cyc_sigma_grad[0])
                    else:
                        grad3.append(wo_cyc_grad3[i])
                else:
                    grad3.append(wo_cyc_grad3[i]+sf_grad3[i]+tf_grad3[i])
            for i in range(len(new_biases)):
                if sf_grad4[i] == None:
                    grad4.append(wo_cyc_grad4[i])
                else:
                    grad4.append(wo_cyc_grad4[i]+sf_grad4[i]+tf_grad4[i])
            train_op3 = op3.apply_gradients(list(zip(grad3,new_weights)))
            train_op4 = op4.apply_gradients(list(zip(grad4,new_biases)))
            #train_op3=tf.train.MomentumOptimizer(learning_rate*1.0,0.9).minimize(self.F_loss, var_list=new_weights)
            #train_op4=tf.train.MomentumOptimizer(learning_rate*2.0,0.9).minimize(self.F_loss, var_list=new_biases)
        self.s_cyc_grad = tf.gradients(self.cycleloss, self.source_feature)
        self.t_cyc_grad = tf.gradients(self.cycleloss, self.target_feature)
        self.s_dann_grad = tf.gradients(self.G_loss, self.source_feature)
        self.t_dann_grad = tf.gradients(self.G_loss, self.target_feature)
        self.s_mtr_grad = tf.gradients(self.metricloss, self.source_feature)
        self.t_mtr_grad = tf.gradients(self.metricloss, self.target_feature)
        self.s_pmtr_grad = tf.gradients(self.puremtrloss, self.source_feature)
        #self.t_pmtr_grad = tf.gradients(self.puremtrloss, self.target_feature)
        self.sigma_grad = cyc_sigma_grad
        return tf.group(train_op3,train_op4)

    def assign_op(self):
        embeddings = tf.Variable(tf.zeros([self.batch_size*2,self.featurelen], name="embedding_intial_tensor"), name="real_Embedding")
        self.assignment = embeddings.assign(self.concat_feature)

    def load_original_weights(self, session, skip_layers=[]):
        weights_dict = np.load('bvlc_alexnet.npy', encoding='bytes').item()

        for op_name in weights_dict:
            # if op_name in skip_layers:
            #     continue

            if op_name == 'fc8' and self.num_classes != 1000:
                continue

            with tf.variable_scope('reuse_inference/'+op_name, reuse=True):
                print ('=============================OP_NAME  ========================================')
                for data in weights_dict[op_name]:
                    if len(data.shape) == 1:
                        var = tf.get_variable('biases')
                        print (op_name,var)
                        session.run(var.assign(data))
                    else:
                        var = tf.get_variable('weights')
                        print (op_name,var)
                        session.run(var.assign(data))


"""
Helper methods
"""
def conv(x, filter_height, filter_width, num_filters, stride_y, stride_x, name, bn=False,padding='SAME', groups=1):
    input_channels = int(x.get_shape()[-1])
    convolve = lambda i, k: tf.nn.conv2d(i, k, strides=[1, stride_y, stride_x, 1], padding=padding)

    with tf.variable_scope(name) as scope:
        weights = tf.get_variable('weights', shape=[filter_height, filter_width, input_channels/groups, num_filters],initializer=tf.contrib.layers.xavier_initializer())
        biases = tf.get_variable('biases', shape=[num_filters])

        if groups == 1:
            conv = convolve(x, weights)
        else:
            input_groups = tf.split(axis=3, num_or_size_splits=groups, value=x)
            weight_groups = tf.split(axis=3, num_or_size_splits=groups, value=weights)
            output_groups = [convolve(i, k) for i,k in zip(input_groups, weight_groups)]
            conv = tf.concat(axis=3, values=output_groups)

        bias = tf.reshape(tf.nn.bias_add(conv, biases), [-1]+conv.get_shape().as_list()[1:])
        if bn==True:
            bias=tf.contrib.layers.batch_norm(bias,scale=True)
        relu = tf.nn.relu(bias, name=scope.name)
        return relu

def D(x):
    with tf.variable_scope('D'):
        num_units_in=int(x.get_shape()[-1])
        num_units_out=1
        n=1024
        weights = tf.get_variable('weights',shape=[num_units_in,n],initializer=tf.contrib.layers.xavier_initializer())
        biases = tf.get_variable('biases', shape=[n], initializer=tf.zeros_initializer())
        hx=(tf.matmul(x,weights)+biases)
        ax=tf.nn.relu(hx)
            
        weights2 = tf.get_variable('weights2',shape=[n,n],initializer=tf.contrib.layers.xavier_initializer())
        biases2 = tf.get_variable('biases2', shape=[n], initializer=tf.zeros_initializer())
        hx2=(tf.matmul(ax,weights2)+biases2)
        ax2=tf.nn.relu(hx2)
        weights3 = tf.get_variable('weights3',shape=[n,num_units_out],initializer=tf.contrib.layers.xavier_initializer())
        biases3 = tf.get_variable('biases3', shape=[num_units_out], initializer=tf.zeros_initializer())
        hx3=tf.matmul(ax2,weights3)+biases3
        return hx3,tf.nn.sigmoid(hx3)

def fc(x, num_in, num_out, name, relu=True,bn=False,stddev=0.001):
    with tf.variable_scope(name) as scope:
        weights = tf.get_variable('weights', shape=[num_in,num_out],initializer=tf.contrib.layers.xavier_initializer())
        biases = tf.get_variable('biases',initializer=tf.constant(0.1,shape=[num_out]))
        act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)
        if bn==True:
            act=tf.contrib.layers.batch_norm(act,scale=True)
        if relu == True:
            relu = tf.nn.relu(act)
            return relu
        else:
            return act
def leaky_relu(x, alpha=0.2):
    return tf.maximum(tf.minimum(0.0, alpha * x), x)

def outer(a,b):
        a=tf.reshape(a,[-1,a.get_shape()[-1],1])
        b=tf.reshape(b,[-1,1,b.get_shape()[-1]])
        c=a*b
        return tf.contrib.layers.flatten(c)

def max_pool(x, filter_height, filter_width, stride_y, stride_x, name, padding='SAME'):
    return tf.nn.max_pool(x, ksize=[1, filter_height, filter_width, 1], strides = [1, stride_y, stride_x, 1],
                          padding = padding, name=name)

def lrn(x, radius, alpha, beta, name, bias=1.0):
    return tf.nn.local_response_normalization(x, depth_radius=radius, alpha=alpha, beta=beta, bias=bias, name=name)

def dropout(x, keep_prob):
    return tf.nn.dropout(x, keep_prob)
