import logging
import os.path
import numpy as np
import tensorflow as tf

eps = 1e-5
import requests

logger = logging.getLogger(__name__)

def bond_energy(r, k1=1, k2=1):
    return -k1/(r+eps) + k2/(r+eps)/(r+eps)

def get_grad_weight(yt_hat, featurelen):
    if np.isnan(yt_hat).any():
        print('yt_hat is detected to have a nan')
    class_num = yt_hat.shape[1]
    ent = np.sum(-yt_hat * np.log(yt_hat+eps),1)
    #left_idx = (ent<0.5)
    #middle_idx = np.logical_and(ent >= 0.5 , ent < 0.7)
    #right_idx  = (ent >= .7)
    #weight = 4 * ent * ent *left_idx + 25 * (ent - 0.7) * (ent - 0.7) * middle_idx
    weight = ent * np.exp(-ent+1)
    weight = np.tile(np.expand_dims(weight,1),[1,featurelen])
    return weight
    

def maybe_download(url, dest):
    """Download the url to dest if necessary, optionally checking file
    integrity.
    """
    if not os.path.exists(dest):
        logger.info('Downloading %s to %s', url, dest)
        download(url, dest)


def download(url, dest):
    """Download the url to dest, overwriting dest if it already exists."""
    response = requests.get(url, stream=True)
    with open(dest, 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)

#### THINK HOW TO EXPLOIT THE PREDICTED TARGET LABEL ####
def LP_closed(trans_list,l):
    A_ul, A_uu = trans_list
    A_ul = A_ul + eps
    A_uu = A_uu + eps
    node_num_u, node_num_l = A_ul.get_shape().as_list()
    if node_num_u == None:
        node_num = node_num_l
    else:
        node_num = node_num_u
    A = tf.concat([A_ul,A_uu],1)
    D = tf.tile(tf.reduce_sum(A,1,keepdims=True),[1,node_num])
    P_ul = A_ul / D
    P_uu = A_uu / D
    return tf.matmul(tf.matrix_inverse(tf.eye(node_num) - P_uu), tf.matmul(P_ul,l))

def LS_closed(trans_list,l):
    raise NotImplementedError('not yet LS closed form')
def LP_iter(trans_list,l,u):
    A_ul = trans_list[0]
    A_uu = trans_list[1]
    A_ul = A_ul + eps
    A_uu = A_uu + eps
    node_num_u, node_num_l = A_ul.get_shape().as_list()
    if node_num_u == None:
        node_num = node_num_l
    else:
        node_num = node_num_u
    A = tf.concat([A_ul,A_uu],1)
    D = tf.tile(tf.reduce_sum(A,1,keepdims=True),[1,node_num])
    P_ul = A_ul / D
    P_uu = A_uu / D
    return tf.matmul(P_ul,l) + tf.matmul(P_uu, u)
def LS_iter(trans_list,l,u,y0,alpha=0.2):
    A_ll, A_lu, A_ul, A_uu = trans_list
    A_ll += eps
    A_lu += eps
    A_ul += eps
    A_uu += eps
    node_num = A_ul.get_shape().as_list()[1]
    A_l = tf.concat([A_ll,A_lu],1)
    A_u = tf.concat([A_ul,A_uu],1)
    A   = tf.concat([A_l,A_u],0)
    D_inv_sqrt = tf.diag(tf.reciprocal(tf.sqrt(tf.reduce_sum(A,1)+eps)))
    S   = D_inv_sqrt * A * D_inv_sqrt
    y = tf.concat([l,u],0)
    y = alpha * S * y + (1 - alpha) * y0
    return y[:node_num], y[node_num:]

def Quadratic(f,L):
    F = tf.diag_part(tf.matmul(tf.matmul(tf.transpose(f,[1,0]),L),f)/2)
    return tf.reduce_mean(F)

def CE(f_pred, y):
    return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=f_pred, labels=y))

def Source_LP(y, W_SS):
    W_SS += eps
    node_num = W_SS.get_shape().as_list()[1]
    D = tf.tile(tf.reduce_sum(W_SS,1,keepdims=True),[1,node_num])
    P_SS = W_SS / D
    return tf.reduce_mean(tf.reduce_sum(tf.abs(tf.matmul(P_SS, y) - y),1))
   

def conv_lap(y):
    y_fb1 = np.expand_dims(np.transpose(y,[1,0]),2)
    y_f1b = np.transpose(y_fb1,[0,2,1])
    y_fbb = y_fb1 * y_f1b
    M_eq  = np.sum(y_fbb,0)
    M     = -2 * M_eq + 1
    D     = np.diag(np.sum(M,1))
    L     = D - M
    return L
