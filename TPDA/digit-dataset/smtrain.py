import os, sys
from util import *
import numpy as np
import tensorflow as tf
import datetime
#from dgmodel import LeNetModel
from mstnmodel import LeNetModel
#from svhnmodel import SVHNModel
#from drcnmodel import DRCNModel
#sys.path.insert(0, '../../utils')
from mnist import MNIST
from svhn import SVHN
#from usps import USPS
from preprocessing import preprocessing

import math
from tensorflow.contrib.tensorboard.plugins import projector

tf.app.flags.DEFINE_float('learning_rate', 1e-2, 'Learning rate for adam optimizer')
tf.app.flags.DEFINE_float('dropout_keep_prob', 0.5, 'Dropout keep probability')
tf.app.flags.DEFINE_integer('num_epochs', 100000, 'Number of epochs for training')
tf.app.flags.DEFINE_integer('batch_size', 128, 'Batch size')
tf.app.flags.DEFINE_string('train_layers', 'fc8,fc7,fc6,conv5,conv4,conv3,conv2,conv1', 'Finetuning layers, seperated by commas')
tf.app.flags.DEFINE_string('multi_scale', '256,257', 'As preprocessing; scale the image randomly between 2 numbers and crop randomly at networs input size')
tf.app.flags.DEFINE_string('train_root_dir', '/home/omega/BigData_Result/TPDA/digit', 'Root directory to put the training data')
tf.app.flags.DEFINE_integer('log_step', 10000, 'Logging period in terms of iteration')
tf.app.flags.DEFINE_boolean('LP_closed_form',True,'If true : use closed form conatining inverse else : perform k iteraion. k is determined by LP_iter hyperparameter')
tf.app.flags.DEFINE_string('LP_or_LS','LP','LP : Lable propagation LS : Label spreading')
tf.app.flags.DEFINE_integer('LP_iter',7,'take only when LP_closed_form is Flase, perform random walk k steps')
tf.app.flags.DEFINE_string('metric_form','CE','choose among 1)quadratic 2)CE (cross_entropy)')
tf.app.flags.DEFINE_boolean('source_recon_loss',False,'penaly for metric being incompatible with source label')
tf.app.flags.DEFINE_string('distance_form','euclidean','choose among 1)euclidean 2)L1 3)correlation 4)log_correlation 5)inner_product')
tf.app.flags.DEFINE_float('sigma',1,'hyperparameter for guasssian distance')
tf.app.flags.DEFINE_float('LS_alpha',0.1,'LS alpha')
tf.app.flags.DEFINE_float('f_norm_lambda',1,'coefficient of norm of the feature in metric loss')
NUM_CLASSES = 10

TRAIN_FILE='svhn'
TEST_FILE='mnist'
print (TRAIN_FILE+'  --------------------------------------->   '+TEST_FILE)
print (TRAIN_FILE+'  --------------------------------------->   '+TEST_FILE)
print (TRAIN_FILE+'  --------------------------------------->   '+TEST_FILE)



TRAIN=SVHN('/home/omega/BigData_Result/svhn',split='train',shuffle=True)
VALID=MNIST('/home/omega/BigData_Result/mnist',split='test',shuffle=True)
TEST=MNIST('/home/omega/BigData_Result/mnist',split='test',shuffle=False)
#TEST_INDUCTIVE=MNIST('/home/omega/BigData_Result/mnist',split='valid',shuffle=False)

FLAGS = tf.app.flags.FLAGS
MAX_STEP=50000

def decay(start_rate,epoch,num_epochs):
        return start_rate/pow(1+0.001*epoch,0.75)

def adaptation_factor(x):
    #return 1.0
    #return 0.25
    den=1.0+math.exp(-10*x)
    lamb=2.0/den-1.0
    return min(lamb,1.0)
def main(_):
    # Create training directories
    now = datetime.datetime.now()
    train_dir_name = now.strftime('alexnet_%Y%m%d_%H%M%S')
    train_dir = os.path.join(FLAGS.train_root_dir, train_dir_name)
    checkpoint_dir = os.path.join(train_dir, 'checkpoint')
    tensorboard_dir = os.path.join(train_dir, 'tensorboard')
    tensorboard_train_dir = os.path.join(tensorboard_dir, 'train')
    tensorboard_val_dir = os.path.join(tensorboard_dir, 'val')
    log_dir = os.path.join(train_dir,'log')

    if not os.path.isdir(FLAGS.train_root_dir): os.mkdir(FLAGS.train_root_dir)
    if not os.path.isdir(train_dir): os.mkdir(train_dir)
    if not os.path.isdir(checkpoint_dir): os.mkdir(checkpoint_dir)
    if not os.path.isdir(tensorboard_dir): os.mkdir(tensorboard_dir)
    if not os.path.isdir(tensorboard_train_dir): os.mkdir(tensorboard_train_dir)
    if not os.path.isdir(tensorboard_val_dir): os.mkdir(tensorboard_val_dir)
    if not os.path.isdir(log_dir): os.mkdir(log_dir)

    # Write flags to txt
    flags_file_path = os.path.join(train_dir, 'flags.txt')
    flags_file = open(flags_file_path, 'w')
    flags_file.write('learning_rate={}\n'.format(FLAGS.learning_rate))
    flags_file.write('dropout_keep_prob={}\n'.format(FLAGS.dropout_keep_prob))
    flags_file.write('num_epochs={}\n'.format(FLAGS.num_epochs))
    flags_file.write('batch_size={}\n'.format(FLAGS.batch_size))
    flags_file.write('train_layers={}\n'.format(FLAGS.train_layers))
    flags_file.write('multi_scale={}\n'.format(FLAGS.multi_scale))
    flags_file.write('train_root_dir={}\n'.format(FLAGS.train_root_dir))
    flags_file.write('log_step={}\n'.format(FLAGS.log_step))
    flags_file.write('LP_closed_from={}\n'.format(FLAGS.LP_closed_form))
    flags_file.write('LP_or_LS={}\n'.format(FLAGS.LP_or_LS))
    flags_file.write('LP_iter={}\n'.format(FLAGS.LP_iter))
    flags_file.write('metric_form={}\n'.format(FLAGS.metric_form))
    flags_file.write('source_recon_loss={}\n'.format(FLAGS.source_recon_loss))
    flags_file.write('distance_form={}\n'.format(FLAGS.distance_form))
    flags_file.write('sigma={}\n'.format(FLAGS.sigma))
    flags_file.write('LS_alpha={}\n'.format(FLAGS.LS_alpha))
    flags_file.write('f_norm_lambda{}\n'.format(FLAGS.f_norm_lambda))
    flags_file.close()
  
    featurelen = 10 
    with tf.variable_scope('reuse_inference/theta') as scope:
        sigma = tf.get_variable('weights', shape=[featurelen],initializer=tf.constant_initializer(1.))
    #sigma = tf.placeholder(tf.float32,[1],name='maximum_distance') 
    adlamb=tf.placeholder(tf.float32,name='adlamb')
    num_update=tf.placeholder(tf.float32,name='num_update')
    decay_learning_rate=tf.placeholder(tf.float32)
    dropout_keep_prob = tf.placeholder(tf.float32)
    is_training=tf.placeholder(tf.bool)    
    time=tf.placeholder(tf.float32,[1])

    # Model
    train_layers = FLAGS.train_layers.split(',')
    model = LeNetModel(num_classes=NUM_CLASSES, image_size=28,is_training=is_training,dropout_keep_prob=dropout_keep_prob,sigma=sigma,FLAGS=FLAGS)
    # Placeholders
    target_feature_grad_weight = tf.placeholder(tf.float32, [None,featurelen])
    x_s = tf.placeholder(tf.float32, [FLAGS.batch_size, 32, 32, 3],name='x')
    x_t = tf.placeholder(tf.float32, [None, 28, 28, 1],name='xt')
    x=preprocessing(x_s,model)
    xt=preprocessing(x_t,model)
    tf.summary.image('Source Images',x)
    tf.summary.image('Target Images',xt)
    print ('x_s ',x_s.get_shape())
    print ('x ',x.get_shape())
    print ('x_t ',x_t.get_shape())
    print ('xt ',xt.get_shape())
    y = tf.placeholder(tf.float32, [FLAGS.batch_size, NUM_CLASSES],name='y')
    yt = tf.placeholder(tf.float32, [None, NUM_CLASSES],name='yt')
    class_Lap = tf.placeholder(tf.float32, [FLAGS.batch_size, FLAGS.batch_size], 'class_laplacian')
    loss = model.loss(x, y)
    # Training accuracy of the model
    source_correct_pred = tf.equal(tf.argmax(model.score, 1), tf.argmax(y, 1))
    source_correct=tf.reduce_sum(tf.cast(source_correct_pred,tf.float32))
    source_accuracy = tf.reduce_mean(tf.cast(source_correct_pred, tf.float32))
    
    G_loss,D_loss,LP_s,LP_t=model.adloss(x,xt,y,FLAGS.f_norm_lambda,class_Lap)
    #G_loss,D_loss,sc,tc=model.adloss(x,xt,y,yt)
    
    # Testing accuracy of the model
    target_correct_pred = tf.equal(tf.argmax(LP_t, 1), tf.argmax(yt, 1))
    target_correct=tf.reduce_sum(tf.cast(target_correct_pred,tf.float32))
    target_accuracy = tf.reduce_mean(tf.cast(target_correct_pred, tf.float32))
    
    update_op = model.optimize(decay_learning_rate,train_layers,adlamb,target_feature_grad_weight)
    
    D_op=model.adoptimize(decay_learning_rate,train_layers)
    model.assign_op()
    #optimizer=tf.group(update_op,D_op)

    train_writer=tf.summary.FileWriter(os.path.join(train_dir,'log/tensorboard'))
    train_writer.add_graph(tf.get_default_graph())
    config=projector.ProjectorConfig()
    embedding=config.embeddings.add()
    embedding.tensor_name=model.concat_feature.name
    domain_label_path = os.path.join(checkpoint_dir,'domain_label.csv')
    domain_path = os.path.join(checkpoint_dir,'domain.csv')
    label_path = os.path.join(checkpoint_dir,'label.tsv')
    embedding.metadata_path = domain_label_path
    projector.visualize_embeddings(train_writer,config)
    tf.summary.scalar('G_loss',model.G_loss)
    tf.summary.scalar('D_loss',model.D_loss)
    #tf.summary.scalar('C_loss',model.loss)
    #tf.summary.scalar('SA_loss',model.Semanticloss)
    #tf.summary.scalar('Training Accuracy',source_accuracy)
    tf.summary.scalar('Testing Accuracy',target_accuracy)
    merged=tf.summary.merge_all()




    print ('============================GLOBAL TRAINABLE VARIABLES ============================')
    print (tf.trainable_variables())
    #print '============================GLOBAL VARIABLES ======================================'
    
    #print tf.global_variables()

    gpu_config = tf.ConfigProto()
    gpu_config.gpu_options.allow_growth = True
    
    with tf.Session(config=gpu_config) as sess:
        sess.run(tf.global_variables_initializer())
        saver=tf.train.Saver()
        #saver.restore(sess,'log/checkpoint')
        # Load the pretrained weights
        #model.load_original_weights(sess, skip_layers=train_layers)
        train_writer.add_graph(sess.graph)
        # Directly restore (your model should be exactly the same with checkpoint)
        # saver.restore(sess, "/Users/dgurkaynak/Projects/marvel-training/alexnet64-fc6/model_epoch10.ckpt")

        print("{} Start training...".format(datetime.datetime.now()))
        #sigma_val = 3.
        gd=0
        step = 1
        for epoch in range(300000):
            # Start training
            gd+=1
            lamb=adaptation_factor(gd*1.0/MAX_STEP)
            #rate=decay(FLAGS.learning_rate,gd,MAX_STEP)
            power=gd/10000        
            rate=FLAGS.learning_rate
            tt=pow(0.1,power)
            batch_xs, batch_ys = TRAIN.next_batch(FLAGS.batch_size)
            Laplacian = conv_lap(batch_ys)
            Tbatch_xs, Tbatch_ys = VALID.next_batch(FLAGS.batch_size)
            #sigma_val = 0
            if FLAGS.distance_form == 'euclidean':
                #grad_SS, grad_TS,grad_TT, yt_hat, M_SS , M_ST, M_TT = sess.run([model.grad_SS,model.grad_TS,model.grad_TT,LP_t,model.M_SS,model.M_ST,model.M_TT],feed_dict={x_s : batch_xs, x_t:Tbatch_xs,dropout_keep_prob:0.5, sigma:[sigma_val],y:batch_ys,is_training:False})
                s_val, grad_SS, grad_TS,grad_TT, yt_hat, M_SS , M_ST, M_TT = sess.run([sigma,model.grad_SS,model.grad_TS,model.grad_TT,LP_t,model.M_SS,model.M_ST,model.M_TT],feed_dict={x_s : batch_xs, x_t:Tbatch_xs,dropout_keep_prob:0.5, y:batch_ys,is_training:False})
                if np.isnan(s_val).any():
                    print("SIGMA NAN")
                    import sys
                    sys.exit()
                #sigma_val = np.percentile(np.concatenate([M_SS,M_ST,M_TT],0),10)
                #print(sigma_val)
                #print(np.max(np.concatenate([M_SS,M_ST,M_TT],0)))
                entropy_weight = get_grad_weight(yt_hat,featurelen) 
            for _ in range(5):
                #_=sess.run(D_op, feed_dict={target_feature_grad_weight:entropy_weight, x_s: batch_xs,x_t: Tbatch_xs,time:[1.0*gd],decay_learning_rate:rate,adlamb:lamb,is_training:True,y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys,class_Lap:Laplacian,sigma:[sigma_val]})
                _=sess.run(D_op, feed_dict={target_feature_grad_weight:entropy_weight, x_s: batch_xs,x_t: Tbatch_xs,time:[1.0*gd],decay_learning_rate:rate,adlamb:lamb,is_training:True,y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys,class_Lap:Laplacian})
                #print('discrimiantor s_dann')
                #print(np.isnan(s_dann_grad[0]).any())
                #print('discriminator t_dann')
                #print(np.isnan(t_dann_grad[0]).any())
                #print('celoss')
                #print(celoss)
            #_,summary,_,yt_hat,y_hat,gloss,dloss,cycloss,mtrloss,pmtrloss=sess.run([model.assignment,merged,update_op,LP_t,LP_s,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], feed_dict={target_feature_grad_weight:entropy_weight, x_s: batch_xs,x_t: Tbatch_xs,time:[1.0*gd],decay_learning_rate:rate,adlamb:lamb,is_training:True,y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys,class_Lap:Laplacian,sigma:[sigma_val]})
            s_cyc_grad, t_cyc_grad, s_dann_grad, t_dann_grad, s_mtr_grad, t_mtr_grad, s_pmtr_grad, sigma_grad, s_val,_,summary,_,yt_hat,y_hat,gloss,dloss,cycloss,mtrloss,pmtrloss=sess.run([model.s_cyc_grad,model.t_cyc_grad, model.s_dann_grad, model.t_dann_grad, model.s_mtr_grad, model.t_mtr_grad, model.s_pmtr_grad, model.sigma_grad, sigma,model.assignment,merged,update_op,LP_t,LP_s,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], feed_dict={target_feature_grad_weight:entropy_weight, x_s: batch_xs,x_t: Tbatch_xs,time:[1.0*gd],decay_learning_rate:rate,adlamb:lamb,is_training:True,y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys,class_Lap:Laplacian})
            #s_val,_,summary,_,yt_hat,y_hat,gloss,dloss,cycloss,mtrloss,pmtrloss=sess.run([sigma,model.assignment,merged,update_op,LP_t,LP_s,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], feed_dict={target_feature_grad_weight:entropy_weight, x_s: batch_xs,x_t: Tbatch_xs,time:[1.0*gd],decay_learning_rate:rate,adlamb:lamb,is_training:True,y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys,class_Lap:Laplacian})

            #print('SS')
            #print(np.isnan(grad_SS[0]).any())
            #print('TS')
            #print(np.isnan(grad_TS[0]).any())
            #print('TT')
            #print(np.isnan(grad_TT[0]).any())
            #print('s_cyc') 
            #print(np.isnan(s_cyc_grad[0]).any())
            #print('t_cyc')
            #print(np.isnan(t_cyc_grad[0]).any())
            #print('s_dann')
            #print(np.isnan(s_dann_grad[0]).any())
            #print('t_dann')
            #print(np.isnan(t_dann_grad[0]).any())
            #print('s_mtr')
            #print(np.isnan(s_mtr_grad[0]).any())
            #print('t_mtr')
            #print(np.isnan(t_mtr_grad[0]).any())
            #print('s_pmtr')
            #print(np.isnan(s_pmtr_grad[0]).any())
            #print('sigma')
            #print(np.isnan(sigma_grad[0]).any())
            train_writer.add_summary(summary,gd)
   
            Last_ys = batch_ys
            Last_yt = Tbatch_ys
 
            step += 1
            if gd%250==1:
                epoch=gd/(72357/100)
                print('=-=-=-=-=-=--=-=-=VALIDATION=-=-=-=-=-=-=-=-=-=-')
                print(np.sum(y_hat[:10],1))
                print(batch_ys[:10] - y_hat[:10])
                print(Tbatch_ys[:10] - yt_hat[:10])
                print(entropy_weight[:10])
                print('sigma: ',s_val)
                print ('lambda: ',lamb)
                print ('rate: ',rate)
                print('cyc grad wo entropy weight norm : ',np.linalg.norm(np.concatenate([s_cyc_grad[0],t_cyc_grad[0]],0)))
                print('DANN grad norm : ',np.linalg.norm(np.concatenate([s_dann_grad[0],t_dann_grad[0]])))
                print('mtr grad norm : ',np.linalg.norm(np.concatenate([s_mtr_grad[0],t_mtr_grad[0]])))
                print('pmtr grad norm : ',np.linalg.norm(s_pmtr_grad[0]))
                print ('Epoch {5:<10} Step {3:<10} cycloss {0:<10} G_loss {1:<10} D_loss {2:<10} mtrloss {4:<10} pmtrloss {6:<10}'.format(cycloss,gloss,dloss,gd,mtrloss,epoch,pmtrloss))
                print("{} Start validation".format(datetime.datetime.now()))
                test_acc = 0.
                source_acc = 0.
                test_count = 0
                print ('test_iter ',len(TEST.labels))
                for _ in range(int(len(TEST.labels)/FLAGS.batch_size * 3)):
                    batch_xs, batch_ys = TRAIN.next_batch(FLAGS.batch_size)
                    batch_tx, batch_ty = TEST.next_batch(FLAGS.batch_size)
                    if FLAGS.distance_form == 'euclidean':
                        M_SS, M_ST, M_TT = sess.run([model.M_SS,model.M_ST,model.M_TT],feed_dict={x_s:batch_xs,x_t:batch_tx,dropout_keep_prob:0.5})
                        sigma_val = max(np.max(M_SS),np.max(M_ST),np.max(M_TT))
            	    #print TEST.pointer,'   ',TEST.shuffle
                    #_,s_acc,acc = sess.run([model.assignment,source_accuracy, target_accuracy], feed_dict={x_s: batch_xs, y:batch_ys, x_t: batch_tx, yt: batch_ty, is_training:False,dropout_keep_prob: 1.,sigma:[sigma_val]})
                    _,s_acc,acc = sess.run([model.assignment,source_accuracy, target_accuracy], feed_dict={x_s: batch_xs, y:batch_ys, x_t: batch_tx, yt: batch_ty, is_training:False,dropout_keep_prob: 1.})
                    test_acc += acc
                    source_acc += s_acc
                    test_count += 1
                    Last_ys = batch_ys
                    Last_yt = batch_ty
                print (source_acc/test_count)
                print (test_acc/test_count)
                test_acc /= test_count
            #if epoch==300:
            #    return
            
                #batch_tx, batch_ty = TEST.next_batch(len(TEST.labels))
            #test_acc=sess.run(accuracy,feed_dict={x_t:batch_tx,y:batch_ty,is_training:False,dropout_keep_prob:1.0})
            #print len(batch_tx)
            #    print("{} Validation Accuracy = {:.4f}".format(datetime.datetime.now(), test_acc))

            if gd%2000==1 and gd>0:
                saver.save(sess,os.path.join(checkpoint_dir,str(gd)+'.ckpt'))
                print('tensorboard --logdir '+ checkpoint_dir)
                with open(domain_label_path,'w') as f:
                    for i in range(FLAGS.batch_size):
                        f.write(str(np.argmax(Last_ys[i]))+'\n')
                    for i in range(FLAGS.batch_size):
                        f.write(str(np.argmax(Last_yt[i])+np.shape(Last_yt)[1])+'\n')
                with open(domain_path,'w') as f:
                    for i in range(FLAGS.batch_size):
                        f.write('0\n')
                    for i in range(FLAGS.batch_size):
                        f.write('1\n')
                with open(label_path,'w') as f:
                    for i in range(FLAGS.batch_size):
                        f.write(str(np.argmax(Last_ys[i]))+'\n')
                    for i in range(FLAGS.batch_size):
                        f.write(str(np.argmax(Last_yt[i]))+'\n')
            #return
            #pass 
            #print("{} Saving checkpoint of model...".format(datetime.datetime.now()))

            #save checkpoint of the model
            #checkpoint_path = os.path.join(checkpoint_dir, 'model_epoch'+str(epoch+1)+'.ckpt')
            #save_path = saver.save(sess, checkpoint_path)

            #print("{} Model checkpoint saved at {}".format(datetime.datetime.now(), checkpoint_path))

            ##### SECOND PHASE FOR INDUCTIVE #####
        


if __name__ == '__main__':
    tf.app.run()
