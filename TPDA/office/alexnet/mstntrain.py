import os, sys
import sys
sys.path.append('../')
import numpy as np
import tensorflow as tf
import datetime
from mstnmodel import AlexNetModel
sys.path.insert(0, '../utils')
from preprocessor import BatchPreprocessor
import math
from utils import *
from tensorflow.contrib.tensorboard.plugins import projector
from sklearn import mixture
import matplotlib.pyplot as plt
import pickle

tf.app.flags.DEFINE_float('learning_rate', 0.01, 'Learning rate for adam optimizer')
tf.app.flags.DEFINE_float('dropout_keep_prob', 0.5, 'Dropout keep probability')
tf.app.flags.DEFINE_integer('num_epochs', 10000000, 'Number of epochs for training')
tf.app.flags.DEFINE_integer('batch_size',320, 'Batch size')
tf.app.flags.DEFINE_string('train_layers', 'fc8,fc7,fc6,conv5,conv4,conv3,conv2,conv1', 'Finetuning layers, seperated by commas')
tf.app.flags.DEFINE_string('multi_scale', '256,257', 'As preprocessing; scale the image randomly between 2 numbers and crop randomly at networs input size')
tf.app.flags.DEFINE_string('train_root_dir', '/home/yjy765/BigData_Result/TPDA/office', 'Root directory to put the training data')
tf.app.flags.DEFINE_integer('log_step', 10000, 'Logging period in terms of iteration')
tf.app.flags.DEFINE_boolean('LP_closed_form',True,'If True : use closed form containing inverse else : perform k iteration. k is another hyper parameter')
tf.app.flags.DEFINE_string('LP_or_LS', 'LP', 'LP : Lable propagation LS : Label spreading')
tf.app.flags.DEFINE_integer('LP_iter',10,'take only when LP_closed_form is False, perform random walk k steps')
tf.app.flags.DEFINE_string('metric_form','CE','choose among 1)quadratic 2)CE (cross entropy)')
tf.app.flags.DEFINE_boolean('source_recon_loss',False,'penalty for metric being incompatible with source label')
tf.app.flags.DEFINE_string('distance_form','euclidean','choose among 1)euclidean 2)L1 3)correlation 4)log_correlation 5)inner_product')
tf.app.flags.DEFINE_float('sigma',1,'hyperparameter for guasssian distance')
tf.app.flags.DEFINE_float('LS_alpha',0.1,'LS alpha')
tf.app.flags.DEFINE_float('f_norm_lambda',1,'coefficient of norm of the feature in metric loss')
tf.app.flags.DEFINE_boolean('load_ckpt',False,'whether to load or not')
tf.app.flags.DEFINE_string('ckpt_dir','alexnet_20180909_022646','ckpt direcotry')
tf.app.flags.DEFINE_boolean('first_phase',True,'whether to execute first transductive phase using source data')
tf.app.flags.DEFINE_boolean('second_phase',False,'whether to execute second phase wo source data')
tf.app.flags.DEFINE_boolean('inductive_phase',False,'whether to execute inductive phase')
tf.app.flags.DEFINE_string('selfcyc_mode','None','choose among 1)None 2)SC (Source Classifier) 3)LP')
tf.app.flags.DEFINE_float('cychyp',1,'cycle loss coefficient hyperparameter')

LOG_PREFIX = '/home/yjy765/BigData_Result/TPDA'
NUM_CLASSES = 31
TRAINING_FILE = '../data/amazon_list_conv.txt'
#TRAINING_FILE = '../data/webcam_list_conv.txt'
VAL_FILE = '../data/webcam_list_conv.txt'
#VAL_FILE = '../data/amazon_list_conv.txt'
FLAGS = tf.app.flags.FLAGS
MAX_STEP=50000
MODEL_NAME='amazo_to_webcam'
#MODEL_NAME='webcam_to_amazon'

print('selfcyc_mode')
print(FLAGS.selfcyc_mode)

def decay(start_rate,epoch,num_epochs):
    return start_rate/pow(1+0.001*epoch,0.75)

def adaptation_factor(x):
    if x>=1.0:
        return 1.0
    den=1.0+math.exp(-10*x)
    lamb=2.0/den-1.0
    return lamb
def main(_):
    # Create training directories
    now = datetime.datetime.now()
    train_dir_name = now.strftime('alexnet_%Y%m%d_%H%M%S')
    if FLAGS.load_ckpt:
        train_dir_name = FLAGS.ckpt_dir
    train_dir = os.path.join(FLAGS.train_root_dir, train_dir_name)
    checkpoint_dir = os.path.join(train_dir, 'checkpoint')
    tensorboard_dir = os.path.join(train_dir, 'tensorboard')
    tensorboard_train_dir = os.path.join(tensorboard_dir, 'train')
    tensorboard_val_dir = os.path.join(tensorboard_dir, 'val')
    log_dir = os.path.join(train_dir,'log')

    if not os.path.isdir(FLAGS.train_root_dir): os.mkdir(FLAGS.train_root_dir)
    if not os.path.isdir(train_dir): os.mkdir(train_dir)
    if not os.path.isdir(checkpoint_dir): os.mkdir(checkpoint_dir)
    if not os.path.isdir(tensorboard_dir): os.mkdir(tensorboard_dir)
    if not os.path.isdir(tensorboard_train_dir): os.mkdir(tensorboard_train_dir)
    if not os.path.isdir(tensorboard_val_dir): os.mkdir(tensorboard_val_dir)
    if not os.path.isdir(log_dir): os.mkdir(log_dir)

    # Write flags to txt
    if not FLAGS.load_ckpt:
        flags_file_path = os.path.join(train_dir, 'flags.txt')
        flags_file = open(flags_file_path, 'w')
        flags_file.write('MODEL_NAME={}\n'.format(MODEL_NAME))
        flags_file.write('learning_rate={}\n'.format(FLAGS.learning_rate))
        flags_file.write('dropout_keep_prob={}\n'.format(FLAGS.dropout_keep_prob))
        flags_file.write('num_epochs={}\n'.format(FLAGS.num_epochs))
        flags_file.write('batch_size={}\n'.format(FLAGS.batch_size))
        flags_file.write('train_layers={}\n'.format(FLAGS.train_layers))
        flags_file.write('multi_scale={}\n'.format(FLAGS.multi_scale))
        flags_file.write('train_root_dir={}\n'.format(FLAGS.train_root_dir))
        flags_file.write('log_step={}\n'.format(FLAGS.log_step))
        flags_file.write('LP_closed_from={}\n'.format(FLAGS.LP_closed_form))
        flags_file.write('LP_or_LS={}\n'.format(FLAGS.LP_or_LS))
        flags_file.write('LP_iter={}\n'.format(FLAGS.LP_iter))
        flags_file.write('metric_form={}\n'.format(FLAGS.metric_form))
        flags_file.write('source_recon_loss={}\n'.format(FLAGS.source_recon_loss))
        flags_file.write('distance_form={}\n'.format(FLAGS.distance_form))
        flags_file.write('sigma={}\n'.format(FLAGS.sigma))
        flags_file.write('LS_alpha={}\n'.format(FLAGS.LS_alpha))
        flags_file.write('f_norm_lambda{}\n'.format(FLAGS.f_norm_lambda))
        flags_file.write('selfcyc_mode{}\n'.format(FLAGS.selfcyc_mode))
        flags_file.write('cychyp{}\n'.format(FLAGS.cychyp))
        flags_file.close()

    featurelen = 256
    with tf.variable_scope('reuse_inference/theta') as scope:
        sigma = tf.get_variable('weights',shape=[featurelen],initializer=tf.constant_initializer(1.))
        #sigma = tf.get_variable('sigma/weights',shape=[featurelen],initializer=tf.constant_initializer(3.))
        #prior_mean = tf.get_variable('prior/mean/weights',shape=[NUM_CLASSES,featurelen],initializer=tf.constant_initializer(1.))
        #prior_cov = tf.get_variable('prior/cov/weights',shape=[NUM_CLASSES,featurelen],initializer=tf.constant_initializer(1.))
        #prior_w = tf.get_variable('prior/weights', shape=[NUM_CLASSES],initializer=tf.constant_initializer(1.))    
    # Placeholders
    logsumexp_max = tf.placeholder(tf.float32,[1,FLAGS.batch_size])
    x = tf.placeholder(tf.float32, [FLAGS.batch_size, 227, 227, 3],'x')
    xt = tf.placeholder(tf.float32, [None, 227, 227, 3],'xt')
    y = tf.placeholder(tf.float32, [FLAGS.batch_size, NUM_CLASSES],'y')
    yt = tf.placeholder(tf.float32, [None, NUM_CLASSES],'yt')
    selfcyc_xt = tf.placeholder(tf.float32, [None, 227, 227, 3], 'selfcyc_xt')
    selfcyc_yt = tf.placeholder(tf.float32, [None, NUM_CLASSES],'selfcyc_yt')
    cyc_coeff  = tf.placeholder(tf.float32)
    dann_coeff = tf.placeholder(tf.float32)
    mtr_coeff  = tf.placeholder(tf.float32)
    class_Lap = tf.placeholder(tf.float32, [FLAGS.batch_size, FLAGS.batch_size], 'class_laplacian')
    adlamb=tf.placeholder(tf.float32)
    decay_learning_rate=tf.placeholder(tf.float32)
    dropout_keep_prob = tf.placeholder(tf.float32)
    target_feature_grad_weight = tf.placeholder(tf.float32,[None,featurelen])
    tf_selfcyc_grad_weight = tf.placeholder(tf.float32,[None,featurelen])
    slf_selfcyc_grad_weight = tf.placeholder(tf.float32,[None,featurelen])
    #target_feature_entropy     = tf.placeholder(tf.float32,[None,featurelen])
    lognorm_first = tf.placeholder(tf.float32,[1])
    lognorm_second = tf.placeholder(tf.float32,[1])

    ### feature grad weight from target feature entropy ###
    ### use with adaptive entropy_coeff ###
    #target_adaptive_feature_grad = entropy_coeff * target_feature_entropy * tf.exp(-entropy_coeff * target_feature_entropy + 1)

    # Model
    train_layers = FLAGS.train_layers.split(',')
    model = AlexNetModel(num_classes=NUM_CLASSES, dropout_keep_prob=dropout_keep_prob, sigma=sigma, FLAGS=FLAGS)
    ### YET WE MAY NOT NEED FOLLOWING LOSS. NEED ONLY WHEN METRIC FORM IS CE OR TO COMPARE TRANSDUCTIVE/SECOND-PHASE CLASSIFIER WITH SOURCE_CLASSIFIER
    ### HOWEVER, WE WOULD DISTINGUISH LOSS FOR WHEN METRIC FORM IS CE AND WHEN COMPARING TR/SECOND-PAHSE
    ### TO COMPARE SECOND-PHASE CLASSIFIER WITH SOURCE_CLASSIFIER, WE SHOULD FIX OTHER VARIABLES BUT SOURCE_CLASSIFIER PARAMETERS(fc9) DURING TRAINING SOURCE_CLASSIFIER
    ### source_classifier_loss : optimize only finetune layer --- using learned feature ###
    source_classifier_loss = model.loss(x, y)

    # Training accuracy of the model
    ### NO MOMENTUM MATCHING
    #G_loss,D_loss,LP_s,LP_t=model.adloss(x,xt,y,FLAGS.f_norm_lambda,class_Lap)
    ### MOMENTUM MATCHING
    G_loss,D_loss,LP_s,LP_t, cyc_LP_s, cyc_LP_t =model.adloss(x,xt,y,FLAGS.f_norm_lambda,class_Lap,selfcyc_xt,selfcyc_yt,lognorm_first=lognorm_first,lognorm_second=lognorm_second,global_step=adlamb)
    source_correct_pred = tf.equal(tf.argmax(model.source_score, 1), tf.argmax(y, 1))
    source_correct=tf.reduce_sum(tf.cast(source_correct_pred,tf.float32))
    source_accuracy = tf.reduce_mean(tf.cast(source_correct_pred, tf.float32))

    target_source_classifier_correct_pred = tf.equal(tf.argmax(model.target_pred,1), tf.argmax(yt,1))
    target_source_classifier_accuracy     = tf.reduce_mean(tf.cast(target_source_classifier_correct_pred, tf.float32))

    target_correct_pred = tf.equal(tf.argmax(LP_t, 1), tf.argmax(yt, 1))
    target_correct=tf.reduce_sum(tf.cast(target_correct_pred,tf.float32))
    target_accuracy = tf.reduce_mean(tf.cast(target_correct_pred, tf.float32))
    if FLAGS.first_phase:
        update_op = model.optimize(decay_learning_rate, train_layers,adlamb,target_feature_grad_weight,tf_selfcyc_grad_weight,slf_selfcyc_grad_weight,cyc_coeff,dann_coeff,mtr_coeff)
   
        D_op=model.adoptimize(decay_learning_rate,train_layers)
    elif FLAGS.second_phase:
        ## SECOND PHASE OPTIMIZER ##
        second_LP_s,second_LP_t = model.second_loss(x,xt,y,FLAGS.f_norm_lambda)
        second_update_op = model.second_optimize(decay_learning_rate, train_layers,target_feature_grad_weight,cyc_coeff,mtr_coeff) 
    assert(FLAGS.first_phase!=FLAGS.second_phase,'in the current situation, we only consider only one of the first phase and second phase')   

    ## TARGET INFERENCE MODEL DEFINE
    target_model = AlexNetModel(num_classes=NUM_CLASSES, dropout_keep_prob=dropout_keep_prob, sigma=sigma, FLAGS=FLAGS,Target_CF=True)
    target_loss = target_model.loss(xt,yt)
    target_update_op = target_model.target_optimize(decay_learning_rate)
    final_correct_pred = tf.equal(tf.argmax(target_model.score, 1), tf.argmax(yt,1))
    final_accuracy = tf.reduce_mean(tf.cast(final_correct_pred, tf.float32))

    if FLAGS.first_phase:
        model.assign_op()
    elif FLAGS.second_phase:
        model.second_assign_op()

    train_writer=tf.summary.FileWriter(os.path.join(train_dir,'./log/tensorboard'))
    train_writer.add_graph(tf.get_default_graph())
    config = projector.ProjectorConfig()
    embedding=config.embeddings.add()
    embedding.tensor_name=model.concat_feature.name
    domain_label_path = os.path.join(checkpoint_dir,'domain_label.csv')
    domain_path = os.path.join(checkpoint_dir,'domain.csv')
    first_phase_LP_path = os.path.join(checkpoint_dir,'LP.csv')
    second_domain_label_path = os.path.join(checkpoint_dir,'second_phase','domain_label.csv')
    second_gt_path = os.path.join(checkpoint_dir,'second_phase','gt.csv')
    label_path = os.path.join(checkpoint_dir,'label.tsv')
    embedding.metadata_path = domain_label_path
    projector.visualize_embeddings(train_writer,config)    
    tf.summary.scalar('Testing Accuracy',target_accuracy)
    merged=tf.summary.merge_all()

    transductive_variables = [v for v in tf.trainable_variables() if v.name.split('/')[0] in ['reuse_inference'] or 'real_Embedding' in v.name]
    print('===========================TRANSDUCTIVE VARIABLES $$$$$$$$$$$$$$$$$$$$')
    print(transductive_variables)
    print ('============================GLOBAL TRAINABLE VARIABLES ============================')
    print (tf.trainable_variables())
    #print '============================GLOBAL VARIABLES ======================================'
    #print tf.global_variables()
    # Batch preprocessors
    multi_scale = FLAGS.multi_scale.split(',')
    if len(multi_scale) == 2: 
        multi_scale = [int(multi_scale[0]), int(multi_scale[1])]
    else:
        multi_scale = None
    print ('==================== MULTI SCALE===================================================')
    print (multi_scale)
    train_preprocessor = BatchPreprocessor(dataset_file_path=TRAINING_FILE, num_classes=NUM_CLASSES,
                                           output_size=[227, 227], horizontal_flip=True, shuffle=True, multi_scale=multi_scale)
    Ttrain_preprocessor = BatchPreprocessor(dataset_file_path=VAL_FILE, num_classes=NUM_CLASSES,
                                           output_size=[227, 227], horizontal_flip=True, shuffle=True, multi_scale=multi_scale)
    val_preprocessor = BatchPreprocessor(dataset_file_path=VAL_FILE, num_classes=NUM_CLASSES, output_size=[227, 227],multi_scale=multi_scale,istraining=False,shuffle=True)
    train_preprocessor_for_test = BatchPreprocessor(dataset_file_path=TRAINING_FILE, num_classes=NUM_CLASSES,
                                            output_size=[227,227], horizontal_flip=True, shuffle=True, multi_scale=multi_scale)
    test_preprocessor = BatchPreprocessor(dataset_file_path=VAL_FILE, num_classes=NUM_CLASSES, output_size=[227,227],multi_scale=multi_scale,istraining=False)
    pretraining_batch = BatchPreprocessor(dataset_file_path=TRAINING_FILE, num_classes=NUM_CLASSES, output_size=[227,227], horizontal_flip=True, shuffle=False, multi_scale=multi_scale)
    for_selfcyc_batch = BatchPreprocessor(dataset_file_path=TRAINING_FILE, num_classes=NUM_CLASSES, output_size=[227,227], horizontal_flip=True, shuffle=True, multi_scale=multi_scale)

    # Get the number of training/validation steps per epoch
    train_batches_per_epoch = np.floor(len(train_preprocessor.labels) / FLAGS.batch_size).astype(np.int16)
    Ttrain_batches_per_epoch = np.floor(len(Ttrain_preprocessor.labels) / FLAGS.batch_size).astype(np.int16)
    val_batches_per_epoch = np.floor(len(val_preprocessor.labels) / FLAGS.batch_size).astype(np.int16)

    gpu_config = tf.ConfigProto()
    gpu_config.gpu_options.allow_growth= True
    
    first_phase_acc = []
    first_phase_cyc = [] 
    with tf.Session(config=gpu_config) as sess:
        sess.run(tf.global_variables_initializer())
        print(transductive_variables)
        saver=tf.train.Saver(transductive_variables)
        train_writer.add_graph(sess.graph)
        if FLAGS.load_ckpt:
            ckpt=tf.train.get_checkpoint_state(checkpoint_dir)
            if ckpt and ckpt.model_checkpoint_path:
                print('LOAD-------------xxxxxxxxxxx')
                saver.restore(sess,ckpt.model_checkpoint_path)
        else:
            # Load the pretrained weights
            model.load_original_weights(sess, skip_layers=train_layers)

        print('first phase')
        print(FLAGS.first_phase)
        if FLAGS.first_phase:
            #### GMM initialize ####
            pretrained_feature_X = np.zeros((train_batches_per_epoch*FLAGS.batch_size,featurelen))
            for i in range(train_batches_per_epoch):
              batch_xs, batch_ys, _ = pretraining_batch.next_batch(FLAGS.batch_size)
              s_feature = sess.run(model.source_feature,feed_dict={x:batch_xs,dropout_keep_prob:1.0})
              pretrained_feature_X[i*FLAGS.batch_size:(i+1)*FLAGS.batch_size] = s_feature
            gmm = mixture.GaussianMixture(n_components=NUM_CLASSES, covariance_type='diag', max_iter=1000).fit(pretrained_feature_X)
            gmm_init_w = gmm.weights_
            gmm_init_mean = gmm.means_
            gmm_init_cov = gmm.covariances_
            #model.assign_prior_op(prior_mean,prior_cov,prior_w,gmm_init_mean,np.log(gmm_init_cov),gmm_init_w,sess)
            print(np.log(gmm_init_cov))
            print(np.log(gmm_init_w))
            print(gmm.score(pretrained_feature_X))
            # Directly restore (your model should be exactly the same with checkpoint)
            # saver.restore(sess, "/Users/dgurkaynak/Projects/marvel-training/alexnet64-fc6/model_epoch10.ckpt")

            print("{} Start training...".format(datetime.datetime.now()))
            print("{} Open Tensorboard at --logdir {}".format(datetime.datetime.now(), tensorboard_dir))
            gd=0
            source_test_iter = 0
            target_test_iter = 0
        
            cyc_coeff_val = 1.
            dann_coeff_val = 1.
            mtr_coeff_val = 1.
            for epoch in range(MAX_STEP):
                #print("{} Epoch number: {}".format(datetime.datetime.now(), epoch+1))
                step = 1
                # Start training
                while step < train_batches_per_epoch:
                    gd+=1
                    lamb = adaptation_factor(gd*1.0/MAX_STEP)
                    rate=decay(FLAGS.learning_rate,gd,MAX_STEP)
                
                    if gd%Ttrain_batches_per_epoch==0:
                        Ttrain_preprocessor.reset_pointer()
                    if gd%train_batches_per_epoch==0:
                        train_preprocessor.reset_pointer()
                    batch_xs, batch_ys, _ = train_preprocessor.next_batch(FLAGS.batch_size)
                    Laplacian = conv_lap(batch_ys)
                    Tbatch_xs, Tbatch_ys, _ = Ttrain_preprocessor.next_batch(FLAGS.batch_size)
                    selfcyc_xs, selfcyc_ys, _ = Ttrain_preprocessor.next_batch(FLAGS.batch_size)
                    #### F NORM ###
                    source_r, s_val, yt_hat = sess.run([model.source_radius, sigma, LP_t], feed_dict={x:batch_xs,xt:Tbatch_xs,dropout_keep_prob:1.0, y:batch_ys})
                    #### GMM ####
                    #ep, source_r, s_val, yt_hat = sess.run([model.ep, model.source_radius, sigma,LP_t],feed_dict={x: batch_xs,xt:Tbatch_xs,dropout_keep_prob:1.0, y: batch_ys})
                    ## get mu and prior from source_r and first, second momentum from them ##
                    first_mom, second_mom = get_lognormal_momentum(source_r,1.0)
                    #ep_val = np.max(ep,0,keepdims=True)
                    if np.isnan(s_val).any():
                        print('s val nan')
                        sys.exit()
                    entropy_weight = get_grad_weight(yt_hat,featurelen)
                    if FLAGS.selfcyc_mode == 'None':
                        selfcyc_ent_val = np.zeros((FLAGS.batch_size,featurelen))
                    elif FLAGS.selfcyc_mode == 'SC':
                        #### GET SOURCE CLASSIFIER PRED FOR SELFCYC ####
                        selfcyc_yt_val = sess.run(model.target_pred, feed_dict={xt:selfcyc_xs,dropout_keep_prob:1.0})
                        selfcyc_ent_val = get_grad_weight(selfcyc_yt_val,featurelen)
                    elif FLAGS.selfcyc_mode == 'LP':
                        #### GET LP LABEL FOR SELFCYC ####
                        selfcyc_yt_val = sess.run(LP_t, feed_dict={x:batch_xs, y: batch_ys, xt:selfcyc_xs, dropout_keep_prob:1.0})
                        selfcyc_ent_val = get_grad_weight(selfcyc_yt_val,featurelen)
                    else:
                        raise NotImplementedError('choose among 1)None 2)SC 3)LP')
                    for _ in range(5):
                        _=sess.run(D_op, feed_dict={x:batch_xs,xt:Tbatch_xs,decay_learning_rate:rate,adlamb:lamb,y:batch_ys,dropout_keep_prob:0.5,class_Lap:Laplacian})
                     
                    #### F NORM
                    if FLAGS.selfcyc_mode == 'None':
                        source_r, s_cyc_grad, t_cyc_grad, s_dann_grad, t_dann_grad, s_mtr_grad, t_mtr_grad, s_pmtr_grad, sigma_grad, s_val,_,summary,_,yt_hat,y_hat,y_prime,gloss,dloss,cycloss,mtrloss,pmtrloss = sess.run([model.source_radius,
                        model.s_cyc_grad,model.t_cyc_grad, model.s_dann_grad, model.t_dann_grad, model.s_mtr_grad, model.t_mtr_grad, model.s_pmtr_grad, model.sigma_grad, sigma,model.assignment,merged,update_op,
                            LP_t,LP_s,model.source_pred,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], 
                                feed_dict={target_feature_grad_weight:entropy_weight,x: batch_xs,xt: Tbatch_xs,yt:Tbatch_ys,adlamb:lamb, decay_learning_rate:rate,
                                            y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys, class_Lap:Laplacian, cyc_coeff:cyc_coeff_val,dann_coeff:dann_coeff_val,mtr_coeff:mtr_coeff_val})
                    else:
                        source_r, s_cyc_grad, t_cyc_grad, s_dann_grad, t_dann_grad, s_mtr_grad, t_mtr_grad, s_pmtr_grad, sigma_grad, s_val,_,summary,_,yt_hat,y_hat,y_prime,gloss,dloss,cycloss,mtrloss,pmtrloss = sess.run([model.source_radius,
                        model.s_cyc_grad,model.t_cyc_grad, model.s_dann_grad, model.t_dann_grad, model.s_mtr_grad, model.t_mtr_grad, model.s_pmtr_grad, model.sigma_grad, sigma,model.assignment,merged,update_op,
                            LP_t,LP_s,model.source_pred,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], 
                                feed_dict={target_feature_grad_weight:entropy_weight,x: batch_xs,xt: Tbatch_xs,yt:Tbatch_ys,adlamb:lamb, decay_learning_rate:rate,
                                            y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys, class_Lap:Laplacian, cyc_coeff:cyc_coeff_val,dann_coeff:dann_coeff_val,mtr_coeff:mtr_coeff_val,
                                            tf_selfcyc_grad_weight:entropy_weight, slf_selfcyc_grad_weight:selfcyc_ent_val, selfcyc_xt:selfcyc_xs, selfcyc_yt:selfcyc_yt_val})

                    #### MOMENTUM MATCHING
                    #source_r, s_cyc_grad, t_cyc_grad, s_dann_grad, t_dann_grad, s_mtr_grad, t_mtr_grad, s_pmtr_grad, sigma_grad, s_val,_,summary,_,yt_hat,y_hat,y_prime,gloss,dloss,cycloss,mtrloss,pmtrloss = sess.run([model.source_radius,
                    #model.s_cyc_grad,model.t_cyc_grad, model.s_dann_grad, model.t_dann_grad, model.s_mtr_grad, model.t_mtr_grad, model.s_pmtr_grad, model.sigma_grad, sigma,model.assignment,merged,update_op,
                    #    LP_t,LP_s,model.source_pred,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], 
                    #        feed_dict={target_feature_grad_weight:entropy_weight,x: batch_xs,xt: Tbatch_xs,yt:Tbatch_ys,adlamb:lamb, decay_learning_rate:rate,
                    #                    y: batch_ys,dropout_keep_prob:0.5,yt:Tbatch_ys, class_Lap:Laplacian, cyc_coeff:cyc_coeff_val,dann_coeff:dann_coeff_val,mtr_coeff:mtr_coeff_val, lognorm_first:first_mom, lognorm_second:second_mom })
                    #### GMM PRIOR
                    #gmp, source_r, s_cyc_grad, t_cyc_grad, s_dann_grad, t_dann_grad, s_mtr_grad, s_pmtr_grad, sigma_grad, s_val,_,summary,_,yt_hat,y_hat,y_prime,gloss,dloss,cycloss,mtrloss,pmtrloss = sess.run([model.gmp, model.source_radius,
                    #model.s_cyc_grad,model.t_cyc_grad, model.s_dann_grad, model.t_dann_grad, model.s_mtr_grad, model.s_pmtr_grad, model.sigma_grad, sigma,model.assignment,merged,update_op,
                    #    LP_t,LP_s,model.source_pred,model.G_loss,model.D_loss,model.cycleloss,model.metricloss,model.puremtrloss], 
                    #        feed_dict={target_feature_grad_weight:entropy_weight,x: batch_xs,xt: Tbatch_xs,yt:Tbatch_ys,adlamb:lamb, decay_learning_rate:rate,
                    #                    y: batch_ys,dropout_keep_prob:1.0,yt:Tbatch_ys, class_Lap:Laplacian, cyc_coeff:cyc_coeff_val,dann_coeff:dann_coeff_val,mtr_coeff:mtr_coeff_val,logsumexp_max:ep_val })
                

                    train_writer.add_summary(summary,gd)
                    Last_ys = batch_ys
                    Last_yt = Tbatch_ys
                    Last_LP_yt = yt_hat
                  
                    cyc_grad_norm = np.linalg.norm(np.concatenate([s_cyc_grad[0],t_cyc_grad[0]],0))
                    dann_grad_norm = np.linalg.norm(np.concatenate([s_dann_grad[0],t_dann_grad[0]],0))
                    #### F NORM
                    mtr_grad_norm = np.linalg.norm(np.concatenate([s_mtr_grad[0],t_mtr_grad[0]],0))
                    #### GMM PRIOR
                    #mtr_grad_norm = np.linalg.norm(s_mtr_grad[0])
                    geo_avg = (cyc_grad_norm * dann_grad_norm * mtr_grad_norm) ** (1./3)
                    median_grad = np.median([cyc_grad_norm,dann_grad_norm,mtr_grad_norm])
                    #alg_avg = (cyc_grad_norm + dann_grad_norm + mtr_grad_norm)/3
                    #cyc_coeff_val = median_grad/cyc_grad_norm
                    #dann_coeff_val = median_grad/dann_grad_norm
                    #mtr_coeff_val  = median_grad/mtr_grad_norm
                    cyc_coeff_val = geo_avg/cyc_grad_norm
                    dann_coeff_val = geo_avg/dann_grad_norm
                    mtr_coeff_val  = geo_avg/mtr_grad_norm
                    #cyc_coeff_val = 1.
                    #dann_coeff_val = 1.
                    #mtr_coeff_val = 1.
                    #cyc_coeff_val = alg_avg/cyc_grad_norm
                    #dann_coeff_val = alg_avg/dann_grad_norm
                    #mtr_coeff_val = alg_avg/mtr_grad_norm
 
                    step += 1
                    if gd%50==1:
                        print ('=================== Step {0:<10} ================='.format(gd))
                        print('source crust values')
                        print(np.mean(source_r))
                        print(np.mean(source_r*source_r))
                        print(first_mom)
                        print(second_mom)
                        #print('log likelihood')
                        #print(gmp)
                        #print('y - yhat')
                        #print(batch_ys[:10] - y_hat[:10])
                        print('yt - ythat')
                        print(-Tbatch_ys[:10] + yt_hat[:10])
                        #print('y - yprime')
                        #print(batch_ys[:10] - y_prime[:10])
                        print('entropy weight')
                        print(entropy_weight[:10])
                        #print('simga : ',s_val)
                        print ('Epoch {5:<10} Step {3:<10} cycloss {0:<10} G_loss {1:<10} D_loss {2:<10} mtrloss {4:<10} pmtrloss {6:<10}'.format(cycloss,gloss,dloss,gd,mtrloss,epoch,pmtrloss))
                        print ('lambda: ',lamb)
                        print ('rate: ',rate)
                        print('cyc grad wo entropy weight norm : ',np.linalg.norm(np.concatenate([s_cyc_grad[0],t_cyc_grad[0]],0)))
                        print('DANN grad norm : ',np.linalg.norm(np.concatenate([s_dann_grad[0],t_dann_grad[0]])))
                        #### F NORM
                        print('mtr grad norm : ',np.linalg.norm(np.concatenate([s_mtr_grad[0],t_mtr_grad[0]])))
                        #### GMM PRIOR
                        #print('mtr grad norm : ',np.linalg.norm(s_mtr_grad[0]))
                        print('pmtr grad norm : ',np.linalg.norm(s_pmtr_grad[0]))
                        #### F NORM
                        print('f norm grad norm : ',np.linalg.norm(np.concatenate([s_mtr_grad[0]-s_pmtr_grad[0],t_mtr_grad[0]])))
                        #### GMM PRIOR
                        #print('f norm grad norm : ',np.linalg.norm(s_mtr_grad[0]-s_pmtr_grad[0]))
                        # Epoch completed, start validation
                        print("{} Start validation".format(datetime.datetime.now()))
                        test_acc = 0.
                        source_acc = 0.
                        test_count = 0
                 
                        for _ in range(val_batches_per_epoch*3):
                            batch_xs, batch_ys, _ = train_preprocessor_for_test.next_batch(FLAGS.batch_size)
                            batch_tx, batch_ty, _ = val_preprocessor.next_batch(FLAGS.batch_size)
                            s_ypred, s_acc,acc = sess.run([model.source_pred,source_accuracy,target_accuracy], feed_dict={x: batch_xs, y:batch_ys, xt: batch_tx, yt: batch_ty, dropout_keep_prob: 1.})
                            source_acc += s_acc
                            test_acc += acc
                            test_count += 1
                            source_test_iter +=1
                            target_test_iter +=1
                            if target_test_iter % val_batches_per_epoch == 0:
                                print('target batch reset pointer')
                                val_preprocessor.reset_pointer()
                            if source_test_iter % train_batches_per_epoch == 0:
                                print('source batch reset pointer')
                                train_preprocessor_for_test.reset_pointer()
                        print(source_acc/test_count)
                        print (test_acc/test_count)
                        #test_acc /= test_count
                        first_phase_acc.append(test_acc/test_count)
                        first_phase_cyc.append(cycloss)
                        plt.figure(gd)
                        plt.plot(first_phase_acc,'r')
                        plt.plot(first_phase_cyc,'b')
                        plt.savefig(os.path.join(checkpoint_dir,'acc.png'))
                        plt.close(gd)
 
                        # Reset the dataset pointers
                        val_preprocessor.reset_pointer()
                        #train_preprocessor.reset_pointer()
                    if gd%100==1 and gd>0:
                        saver.save(sess,os.path.join(checkpoint_dir,str(gd)+'.ckpt'))
                        print('tensorboard --logdir '+ checkpoint_dir)
                        with open(domain_label_path,'w') as f:
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_ys[i]))+'\n')
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_yt[i])+np.shape(Last_yt)[1])+'\n')
                        with open(domain_path,'w') as f:
                            for i in range(FLAGS.batch_size):
                                f.write('0\n')
                            for i in range(FLAGS.batch_size):
                                f.write('1\n')
                        with open(label_path,'w') as f:
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_ys[i]))+'\n')
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_yt[i]))+'\n')
                        with open(first_phase_LP_path,'w') as f:
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_ys[i]))+'\n')
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_LP_yt[i])+np.shape(Last_yt)[1])+'\n') 
                    if gd > 50000:
                        break
        print("{} Start self-labeling from label propagation".format(datetime.datetime.now()))
        test_acc = 0.
        test_count = 0
        self_label_array = np.zeros((len(val_preprocessor.labels),NUM_CLASSES))
        true_label_array = np.zeros((len(val_preprocessor.labels),NUM_CLASSES))
        self_label_count = np.zeros((len(val_preprocessor.labels),))
        i = 0
        val_preprocessor.reset_pointer()
        if os.path.isfile('da.pkl'):
            with open('da.pkl','rb') as f:
                array_list = pickle.load(f)
            self_label_array, true_label_array  = array_list
        else:
            while i < int(len(val_preprocessor.labels)/FLAGS.batch_size * 10) or (self_label_count==0).any():
                if i % int(len(val_preprocessor.labels)/FLAGS.batch_size)==0:
                    print(i/int(len(val_preprocessor.labels)/FLAGS.batch_size))
                batch_xs, batch_ys, _ = train_preprocessor.next_batch(FLAGS.batch_size)
                batch_tx, batch_ty, batch_idx = val_preprocessor.next_batch(FLAGS.batch_size)
                s_pred, yt_hat = sess.run([model.source_pred,LP_t], feed_dict={x:batch_xs,xt:batch_tx,y:batch_ys,yt:batch_ty,dropout_keep_prob:1.0})
                self_label_array[batch_idx,:] = self_label_array[batch_idx,:] + yt_hat
                self_label_count[batch_idx] = self_label_count[batch_idx] + 1
                true_label_array[batch_idx,:] = true_label_array[batch_idx,:] + batch_ty
                test_acc += np.sum(np.argmax(s_pred,1)==np.argmax(batch_ys,1))
                i+=1
            print(test_acc/i/FLAGS.batch_size)
            print(self_label_array[:15,:])
            print(true_label_array[:15,:])
            print(self_label_count[:15])
            print('label propagation average accuracy : %f' % np.mean(np.argmax(self_label_array,1)==np.argmax(true_label_array,1)))
            self_label_array = self_label_array / np.expand_dims(self_label_count,1)
            true_label_array = true_label_array / np.expand_dims(self_label_count,1)
            self_label_array = self_label_array / np.sum(self_label_array,1,keepdims=True)
            true_label_array = true_label_array / np.sum(true_label_array,1,keepdims=True)
            with open('da.pkl','wb') as f:
                pickle.dump([self_label_array, true_label_array],f)
        cyc_coeff_val = 1.
        mtr_coeff_val = 0.001
        if FLAGS.second_phase:
            #### SECOND PHASE ONLY W/ TARGET ####
            for epoch in range(int(MAX_STEP/10)):
                #print("{} Epoch number: {}".format(datetime.datetime.now(), epoch+1))
                step = 1
                # Start training
                gd = 0
                while step < train_batches_per_epoch:
                    gd+=1
                    lamb = adaptation_factor(gd*1.0/MAX_STEP*10)
                    rate=decay(0.001,epoch,MAX_STEP/10)
                    tx_f, ty_f, idx_f = Ttrain_preprocessor.next_batch(FLAGS.batch_size)
                    tx_s, ty_s, idx_s = Ttrain_preprocessor.next_batch(FLAGS.batch_size)
                    labeled_x, labeled_y, unlabeled_x, unlabeled_y, gt_labeled_y, gt_unlabeled_y = split_high_score(self_label_array, true_label_array, tx_f, tx_s, idx_f, idx_s, FLAGS.batch_size)
                    labeled_x, labeled_y, unlabeled_x, unlabeled_y, gt_labeled_y, gt_unlabeled_y = split_random(self_label_array, true_label_array, tx_f, tx_s, idx_f, idx_s, FLAGS.batch_size)
                    ## CONSIDIER FOLLOWING
                    entropy_weight = np.ones((FLAGS.batch_size,featurelen))
                    s_val,_,_,yt_hat,y_hat,cycloss,mtrloss,s_cyc_grad,t_cyc_grad,s_mtr_grad,t_mtr_grad = sess.run([sigma, model.second_assignment, second_update_op, second_LP_t,second_LP_s,model.second_cycleloss,model.second_metricloss,model.second_s_cyc_grad,model.second_t_cyc_grad,model.second_s_mtr_grad,model.second_t_mtr_grad], 
                            feed_dict={target_feature_grad_weight:entropy_weight,x: labeled_x ,xt: unlabeled_x,adlamb:lamb, decay_learning_rate:rate, y: labeled_y ,dropout_keep_prob:0.5,yt:unlabeled_y, cyc_coeff:cyc_coeff_val, mtr_coeff:mtr_coeff_val })
                    #s_val,_,yt_hat,y_hat,cycloss,mtrloss,s_cyc_grad,t_cyc_grad,s_mtr_grad,t_mtr_grad = sess.run([sigma, model.second_assignment, second_LP_t,second_LP_s,model.second_cycleloss,model.second_metricloss,model.second_s_cyc_grad,model.second_t_cyc_grad,model.second_s_mtr_grad,model.second_t_mtr_grad], 
                    #        feed_dict={target_feature_grad_weight:entropy_weight,x: labeled_x ,xt: unlabeled_x,adlamb:lamb, decay_learning_rate:rate, y: labeled_y ,dropout_keep_prob:0.5,yt:unlabeled_y, cyc_coeff:cyc_coeff_val, mtr_coeff:mtr_coeff_val })
 
                    Last_ys    = labeled_y
                    Last_yt    = yt_hat
                    Last_gt_ys = gt_labeled_y
                    Last_gt_yt = gt_unlabeled_y
                    cyc_grad_norm = np.linalg.norm(np.concatenate([s_cyc_grad[0],t_cyc_grad[0]],0))
                    mtr_grad_norm = np.linalg.norm(np.concatenate([s_mtr_grad[0],t_mtr_grad[0]],0))
                    geo_avg = (cyc_grad_norm * mtr_grad_norm) ** (1./2)
                    #cyc_coeff_val = geo_avg/cyc_grad_norm
                    #mtr_coeff_val  = geo_avg/mtr_grad_norm
              
                    step += 1
                    if gd%50==1:
                        print ('=================== Step {0:<10} ================='.format(gd))
                        print('labeled yt - ythat')
                        print(-gt_labeled_y[:10] + labeled_y[:10])
                        print('unlabeled gy - y')
                        print(-gt_unlabeled_y[:10] + unlabeled_y[:10])
                        print('yt - ythat')
                        print(-gt_unlabeled_y[:10] + yt_hat[:10])
                        print ('Epoch {0:<10} Step {1:<10} cycloss {2:<10} mtrloss {3:<10}'.format(epoch,gd,cycloss,mtrloss))
                        print('sigma value')
                        print(s_val)
                        print('cyc grad norm : %.10f' % cyc_grad_norm)
                        print('mtr grad norm : %.10f' % mtr_grad_norm)
                        print("{} Start validation".format(datetime.datetime.now()))
                        wo_LP_s_test_correct = 0.
                        w_LP_s_test_correct = 0.
                        ythat_correct = 0.
                        test_count = 0
                        fixed_correct = 0.
                        unfixed_correct = 0.
                        yhat_correct = 0. 
                        for _ in range(val_batches_per_epoch*3):
                            tx_f, ty_f, idx_f = Ttrain_preprocessor.next_batch(FLAGS.batch_size)
                            tx_s, ty_s, idx_s = Ttrain_preprocessor.next_batch(FLAGS.batch_size)
                            labeled_x, labeled_y, unlabeled_x, unlabeled_y, gt_labeled_y, gt_unlabeled_y = split_high_score(self_label_array, true_label_array, tx_f, tx_s, idx_f, idx_s, FLAGS.batch_size)
                            labeled_x, labeled_y, unlabeled_x, unlabeled_y, gt_labeled_y, gt_unlabeled_y = split_random(self_label_array, true_label_array, tx_f, tx_s, idx_f, idx_s, FLAGS.batch_size)
   
                            yt_hat, y_hat = sess.run([second_LP_t, second_LP_s], feed_dict={x: labeled_x, y:labeled_y, xt: unlabeled_x, yt: unlabeled_y, dropout_keep_prob: 1.})
                            wo_LP_s_test_correct += np.sum(np.argmax(yt_hat,1)==np.argmax(gt_unlabeled_y,1)) + np.sum(np.argmax(labeled_y,1)==np.argmax(gt_labeled_y,1))
                            w_LP_s_test_correct += np.sum(np.argmax(yt_hat,1)==np.argmax(gt_unlabeled_y,1)) + np.sum(np.argmax(y_hat,1)==np.argmax(gt_labeled_y,1))
                            ythat_correct += np.sum(np.argmax(yt_hat,1)==np.argmax(gt_unlabeled_y,1))
                            fixed_correct += np.sum(np.argmax(labeled_y,1)==np.argmax(gt_labeled_y,1))
                            yhat_correct += np.sum(np.argmax(y_hat,1)==np.argmax(gt_labeled_y,1))
                            unfixed_correct += np.sum(np.argmax(unlabeled_y,1)==np.argmax(gt_unlabeled_y,1))
                            test_count += 1
                        print(unfixed_correct/test_count/FLAGS.batch_size)
                        print(ythat_correct/test_count/FLAGS.batch_size)
                        print(fixed_correct/test_count/FLAGS.batch_size)
                        print(yhat_correct/test_count/FLAGS.batch_size)
                        print(test_count)
                        print(wo_LP_s_test_correct/test_count/FLAGS.batch_size/2)
                        print(w_LP_s_test_correct/test_count/FLAGS.batch_size/2)
                    if gd%100==1 and gd>0:
                        print('write saver')
                        if not os.path.isdir(os.path.join(checkpoint_dir,'second_phase')): os.mkdir(os.path.join(checkpoint_dir,'second_phase'))
                        saver.save(sess,os.path.join(checkpoint_dir,'second_phase',str(gd)+'.ckpt'))
                        print('tensorboard --logdir '+ checkpoint_dir)
                        with open(second_domain_label_path,'w') as f:
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_ys[i]))+'\n')
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_yt[i])+np.shape(Last_yt)[1])+'\n')
                        with open(second_gt_path,'w') as f:
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_gt_ys[i]))+'\n')
                            for i in range(FLAGS.batch_size):
                                f.write(str(np.argmax(Last_gt_yt[i])+np.shape(Last_gt_yt)[1])+'\n')
                        print('domain csv write')


        if FLAGS.inductive_phase:
            #### SECOND PHASE FOR INDUCTIVE ####
            print("{} Start self-labeling from label propagation".format(datetime.datetime.now()))
            test_acc = 0.
            source_acc = 0.
            test_sc_acc = 0.
            test_count = 0
            self_label_array = np.zeros((len(test_preprocessor.labels),NUM_CLASSES))
            true_label_array = np.zeros((len(test_preprocessor.labels),NUM_CLASSES))
            self_label_count = np.zeros((len(test_preprocessor.labels),))
            print('test_iter ',len(test_preprocessors.labels))        
            i = 0

         

if __name__ == '__main__':
    tf.app.run()

