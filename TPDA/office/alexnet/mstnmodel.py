"""
Derived from: https://github.com/kratzert/finetune_alexnet_with_tensorflow/
"""
import tensorflow as tf
import numpy as np
import math
import sys
from utils import *

eps_M = 1e-6

def protoloss(sc,tc):
        return tf.reduce_mean(tf.square(sc-tc))

class AlexNetModel(object):

    def __init__(self, num_classes=1000, dropout_keep_prob=0.5, sigma = 1, FLAGS=None, prior_mu = 1.0, prior_sigma_sq = 1.0,Target_CF=False):
        self.num_classes = num_classes
        self.dropout_keep_prob = dropout_keep_prob
        
        # Hyperparameter for gaussian distance
        self.sigma = sigma
        self.featurelen=256
        self.LP_closed_form     = FLAGS.LP_closed_form
        self.LP_iter            = FLAGS.LP_iter
        self.metric_form        = FLAGS.metric_form
        self.source_recon_loss  = FLAGS.source_recon_loss
        self.distance_form      = FLAGS.distance_form
        self.LP_or_LS           = FLAGS.LP_or_LS
        self.batch_size         = FLAGS.batch_size
        self.target_cf          = Target_CF       
        self.FLAGS              = FLAGS 
        # Momentum matching parameters
        self.prior_mu           = prior_mu
        self.prior_sigma_sq     = prior_sigma_sq
        self.lognorm_first_mom  = np.exp(self.prior_mu + self.prior_sigma_sq/2)
        self.lognorm_second_mom = (np.exp(self.prior_sigma_sq)-1)*np.exp(2*self.prior_mu + self.prior_sigma_sq) + self.lognorm_first_mom * self.lognorm_first_mom

    def inference(self, x, training=False):
        # 1st Layer: Conv (w ReLu) -> Pool -> Lrn
        conv1 = conv(x, 11, 11, 96, 4, 4, padding='VALID', name='conv1')
        pool1 = max_pool(conv1, 3, 3, 2, 2, padding='VALID', name='pool1')
        norm1 = lrn(pool1, 1, 1e-5, 0.75, name='norm1')

        # 2nd Layer: Conv (w ReLu) -> Pool -> Lrn with 2 groups
        conv2 = conv(norm1, 5, 5, 256, 1, 1, groups=2, name='conv2')
        pool2 = max_pool(conv2, 3, 3, 2, 2, padding='VALID', name ='pool2')
        norm2 = lrn(pool2, 1, 1e-5, 0.75, name='norm2')

        # 3rd Layer: Conv (w ReLu)
        conv3 = conv(norm2, 3, 3, 384, 1, 1, name='conv3')
        # 4th Layer: Conv (w ReLu) splitted into two groups
        conv4 = conv(conv3, 3, 3, 384, 1, 1, groups=2, name='conv4')

        #conv4_flattened=tf.contrib.layers.flatten(conv4)

        # 5th Layer: Conv (w ReLu) -> Pool splitted into two groups
        conv5 = conv(conv4, 3, 3, 256, 1, 1, groups=2, name='conv5')
        pool5 = max_pool(conv5, 3, 3, 2, 2, padding='VALID', name='pool5')

        # 6th Layer: Flatten -> FC (w ReLu) -> Dropout
        flattened = tf.reshape(pool5, [-1, 6*6*256])
        self.flattened=flattened
        fc6 = fc(flattened, 6*6*256, 4096, name='fc6')
        if training:
            fc6 = dropout(fc6, self.dropout_keep_prob)
        self.fc6=fc6
        # 7th Layer: FC (w ReLu) -> Dropout
        fc7 = fc(fc6, 4096, 4096, name='fc7')
        if training:
            fc7 = dropout(fc7, self.dropout_keep_prob)
        self.fc7=fc7
        # 8th Layer: FC and return unscaled activations (for tf.nn.softmax_cross_entropy_with_logits)
        fc8=fc(fc7,4096,256,relu=False,name='fc8')
        self.fc8=fc8
        self.score = fc(fc8, 256, self.num_classes, relu=False, stddev=0.005,name='fc9')
        self.output=tf.nn.softmax(self.score)
        self.feature=self.fc8
        return self.score
    def wganloss(self,x,xt,batch_size,lam=10.0):
        with tf.variable_scope('reuse_inference') as scope:
            scope.reuse_variables()
            self.inference(x,training=True)
            source_fc6=self.fc6
            source_fc7=self.fc7
            source_fc8=self.fc8
        source_softmax=self.output
        source_output=outer(source_fc7,source_softmax)
        print('SOURCE_OUTPUT: ',source_output.get_shape())
        scope.reuse_variables()
        self.inference(xt,training=True)
        target_fc6=self.fc6
        target_fc7=self.fc7
        target_fc8=self.fc8
        target_softmax=self.output
        target_output=outer(target_fc7,target_softmax)
        print('TARGET_OUTPUT: ',target_output.get_shape())
        with tf.variable_scope('reuse') as scope:
            target_logits,_=D(target_fc8)
            scope.reuse_variables()
            source_logits,_=D(source_fc8)
            eps=tf.random_uniform([batch_size,1],minval=0.0,maxval=1.0)
            X_inter=eps*source_fc8+(1-eps)*target_fc8
            grad = tf.gradients(D(X_inter), [X_inter])[0]
            grad_norm = tf.sqrt(tf.reduce_sum((grad)**2, axis=1))
            grad_pen = lam * tf.reduce_mean((grad_norm - 1)**2)
            D_loss=tf.reduce_mean(target_logits)-tf.reduce_mean(source_logits)+grad_pen
            G_loss=tf.reduce_mean(source_logits)-tf.reduce_mean(target_logits)    
            self.G_loss=G_loss
            self.D_loss=D_loss
        return G_loss,D_loss
    def adloss(self,x,xt,y,lamb,L,selfcyc_xt,selfcyc_yt,yt=None,lognorm_first=None,lognorm_second=None,prior_mean=None,prior_cov=None,prior_w=None,logsumexp_max=None,global_step=None):
        # yt : predicted label Not groud truth
        with tf.variable_scope('reuse_inference') as scope:
            scope.reuse_variables()
            source_score = self.inference(x,training=True)
            self.source_pred = self.output
            source_feature= self.feature
            scope.reuse_variables()
            self.inference(xt,training=True)
            target_feature=self.feature
            target_pred=self.output
            scope.reuse_variables()
            self.inference(selfcyc_xt,training=True)
            selfcyc_feature = self.feature
        with tf.variable_scope('reuse') as scope:
            source_logits,_=D(source_feature)
            scope.reuse_variables()
            target_logits,_=D(target_feature)

        self.source_score = source_score
        self.target_pred=target_pred    
        self.source_feature=source_feature
        self.target_feature=target_feature
        self.selfcyc_feature = selfcyc_feature
        self.concat_feature=tf.concat([source_feature,target_feature],0)    

        W_SS, W_ST, W_TS, W_TT = get_graph_weight(self.distance_form, source_feature, target_feature, self.sigma, self.batch_size)
        CYCW_SS, CYCW_ST, CYCW_TS, CYCW_TT = get_graph_weight(self.distance_form, selfcyc_feature, target_feature, self.sigma, self.batch_size)        

        #### LABEL PROPAGATION ####
        if self.LP_closed_form:
            ###### TODO ######
            if self.LP_or_LS == 'LP':
                yt_hat = LP_closed([W_TS,W_TT],y)
                y_hat  = LP_closed([W_ST,W_SS],yt_hat)
                cyc_yt_hat = LP_closed([CYCW_TS,CYCW_TT],selfcyc_yt)
                cyc_y_hat  = LP_closed([CYCW_ST,CYCW_SS],cyc_yt_hat)
            elif self.LP_or_LS == 'LS':
                yt_hat = LS_closed([W_SS,W_ST,W_TS,W_TT],y)
                y_hat  = LS_closed([W_TT,W_TS,W_ST,W_SS],yt_hat)
            else:
                raise NotImplementedError('LP_or_LS should be among 1) LP or 2) LS')
        else:
            ##### TODO #######
            ##### THINK HOW TO INITIALIZE yt_hat #####
            if self.LP_or_LS == 'LP':
                yt_hat = tf.ones_like(y) / 10.
                y_hat  = tf.ones_like(y) / 10.
                for _ in range(self.LP_iter):
                    yt_hat = LP_iter([W_TS,W_TT],y,yt_hat)
                for _ in range(self.LP_iter):
                    y_hat  = LP_iter([W_ST,W_SS],yt_hat,y_hat)
            elif self.LP_or_LS == 'LS':
                yt_hat = tf.ones_like(y) / 10.
                y_hat  = tf.ones_like(y) / 10.
                y0_ST  = tf.concat([y,yt_hat],0)
                y_LP   = y
                for _ in range(self.LP_iter):
                    y_LP, yt_hat = LS_iter([W_SS,W_ST,W_TS,W_TT],y_LP,yt_hat,y0_ST)
                y0_TS  = tf.concat([yt_hat,y_hat],0)
                yt_hat_TS = yt_hat
                for _ in range(self.LP_iter):
                    yt_hat_TS, y_hat  = LS_iter([W_TT,W_TS,W_ST,W_SS],yt_hat_TS,y_hat,y0_TS)
            else:
                raise NotImplementedError('LP_or_LS should be among 1) LP or 2) LS')
        self.y_hat = y_hat
        self.cyc_y_hat = cyc_y_hat
        self.cycleloss = tf.reduce_mean(tf.reduce_sum(tf.abs(y-y_hat),1)) 
        self.selfcycleloss = tf.reduce_mean(tf.reduce_sum(tf.abs(selfcyc_yt - cyc_y_hat),1))
        ###### TODO ######
        if self.metric_form == 'quadratic':
            self.metricloss = Quadratic(self.source_feature, L) / self.batch_size / 100
        elif self.metric_form == 'CE':
            self.metricloss = CE(source_score, y)
        else:
            raise NotImplementedError('1)quadratic 2)CE')
        
        # puremtrloss means the difference btw source real label and predicted label 
        tf.summary.scalar('pure_metricloss',self.metricloss)
        self.puremtrloss = self.metricloss
        ### radius log normal momentum matching ###
        
        source_radius = tf.sqrt(tf.reduce_sum(source_feature * source_feature, 1, keep_dims=True))
        self.source_radius = source_radius
        target_radius = tf.sqrt(tf.reduce_sum(target_feature * target_feature, 1, keep_dims=True))
        #### log norm momentum matching
        #self.metricloss = self.metricloss + lamb * (tf.square(tf.reduce_mean(source_radius)-tf.reduce_mean(lognorm_first))
        #                                            + tf.square(tf.reduce_mean(target_radius)-tf.reduce_mean(lognorm_first))
        #                                            +tf.square(tf.reduce_mean(tf.square(source_radius))-tf.reduce_mean(lognorm_second))
        #                                            + tf.square(tf.reduce_mean(tf.square(target_radius))-tf.reduce_mean(lognorm_second)))
        ### radius log normal momentum matching with adaptive momentum ###
        #self.metricloss = self.metricloss + lamb * (tf.square(tf.reduce_mean(source_radius)-lognorm_first) 
        #                                            + tf.square(tf.reduce_mean(target_radius)-lognorm_first)
        #                                            +tf.square(tf.reduce_mean(tf.square(source_radius))-lognorm_second) 
        #                                            + tf.square(tf.reduce_mean(tf.square(target_radius))-lognorm_second))
       
        ### f norm ###
        self.metricloss = self.metricloss + lamb * tf.reduce_mean(tf.reduce_mean(source_feature * source_feature + target_feature * target_feature,1)) / 30.0  
        #self.metricloss = self.metricloss + bond_energy(tf.reduce_mean(source_feature * source_feature), k1=10, k2=25) + bond_energy(tf.reduce_mean(target_feature * target_feature), k1=10, k2=25)
        ### gaussian maximum likelihood ###
        #squared_difference = tf.square(tf.expand_dims(source_feature,0)-tf.expand_dims(prior_mean,1))
        #reciprocal_cov = tf.tile(tf.expand_dims(tf.exp(-prior_cov),1),[1,self.batch_size,1])
        #exponent_det_part  = -1/2 * (tf.reduce_sum(squared_difference*reciprocal_cov,2)+tf.tile(tf.reduce_sum(prior_cov,1,keep_dims=True),[1,self.batch_size]))
        #weighted_exponent_det = tf.log(tf.tile(tf.expand_dims(tf.nn.softmax(prior_w),1),[1,self.batch_size]))+exponent_det_part
        #self.ep   = weighted_exponent_det
        #weighted_exponent_det_trick = weighted_exponent_det - tf.tile(logsumexp_max,[self.num_classes,1])
        #gmm_prob        = tf.log(tf.reduce_sum(tf.exp(weighted_exponent_det_trick),0,keep_dims=True))+logsumexp_max
        #self.gmp  = tf.reduce_mean(gmm_prob) - self.featurelen/2 * np.log(2*3.141592)
        #self.metricloss = self.metricloss - tf.reduce_mean(gmm_prob) * global_step

        if self.source_recon_loss:
            self.metricloss += Source_LP(y, W_SS)
    
        tf.summary.scalar('metricloss',self.metricloss)
        tf.summary.scalar('cycleloss',self.cycleloss)

        D_real_loss=tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=target_logits,labels=tf.ones_like(target_logits)))
        D_fake_loss=tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=source_logits,labels=tf.zeros_like(source_logits)))
        self.D_loss=D_real_loss+D_fake_loss
        self.G_loss=-self.D_loss
        tf.summary.scalar('G_loss',self.G_loss)
        tf.summary.scalar('JSD',self.G_loss/2+math.log(2))
    
        #self.G_loss=0.1*self.G_loss
        #self.D_loss=0.1*self.D_loss

        #self.grad_check = tf.gradients(self.cycleloss,[self.W_TS,self.W_TT])

        return self.G_loss,self.D_loss,y_hat, yt_hat, cyc_y_hat, cyc_yt_hat

    def loss(self, batch_x, batch_y=None):
        if self.target_cf:
            with tf.variable_scope('target_inference') as scope:
                y_predict = self.inference(batch_x, training=True)
        else:
            with tf.variable_scope('reuse_inference') as scope:
                y_predict = self.inference(batch_x, training=True)
        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_predict, labels=batch_y))
        tf.summary.scalar('Closs',self.loss)
        return self.loss

    def adoptimize(self,learning_rate,train_layers=[]):
        var_list=[v for v in tf.trainable_variables() if 'D' in v.name]
        D_weights=[v for v in var_list if 'weights' in v.name]
        D_biases=[v for v in var_list if 'biases' in v.name]
        print('=================Discriminator_weights=====================')
        print(D_weights)
        print('=================Discriminator_biases=====================')
        print(D_biases)
    
        self.Dregloss=0.0005*tf.reduce_mean([tf.nn.l2_loss(v) for v in var_list if 'weights' in v.name])
        D_op1 = tf.train.MomentumOptimizer(learning_rate,0.9).minimize(self.D_loss+self.Dregloss, var_list=D_weights)
        D_op2 = tf.train.MomentumOptimizer(learning_rate*2.0,0.9).minimize(self.D_loss+self.Dregloss, var_list=D_biases)
        D_op=tf.group(D_op1,D_op2)
        return D_op

    def target_optimize(self, learning_rate):
        var_list = [v for v in tf.trainable_variables() if v.name.split('/')[0] in ['target_inference']]
        print('========================TARGET INFERENCE VAR LIST ++++++++++++++++++++++++++++++++')
        print(var_list)
        train_op = tf.train.MomentumOptimizer(learning_rate*1.0,0.9).minimize(self.loss, var_list=var_list)
        return train_op

    def optimize(self, learning_rate, train_layers,global_step,tf_grad_weight,tf_selfcyc_grad_weight,slf_selfcyc_grad_weight,cyc_coeff,dann_coeff,mtr_coeff):
        print('++++++++++++++++++++++Train layers++++++++++++++++++++++')
        print(train_layers)
        var_list = [v for v in tf.trainable_variables() if v.name.split('/')[1] in train_layers+['fc9','theta']]
        finetune_list=[v for v in var_list if v.name.split('/')[1] in ['conv1','conv2','conv3','conv4','conv5','fc6','fc7']]
        new_list=[v for v in var_list if v.name.split('/')[1] in ['fc8','fc9','theta']]
        self.Gregloss=0.0005*tf.reduce_mean([tf.nn.l2_loss(x) for x in var_list if 'weights' in x.name])
    
        finetune_weights=[v for v in finetune_list if 'weights' in v.name]
        finetune_biases=[v for v in finetune_list if 'biases' in v.name]
        new_weights=[v for v in new_list if 'weights' in v.name]
        new_biases=[v for v in new_list if 'biases' in v.name]

        print('++++++++++++++++++++++New_weights+++++++++++++++++++++')
        print(new_weights)

        for i in range(len(new_weights)):
            if new_weights[i].name.split('/')[1] == 'theta':
                if new_weights[i].name.split('/')[2] == 'entropy_coeff':
                    ent_coeff_idx = i
                elif new_weights[i].name.split('/')[2] == 'sigma':
                    sig_idx = i
                else:
                    sig_idx = i

        print(sig_idx)
        #print(ent_coeff_idx)
    
        print('==============finetune_weights=======================')
        print(finetune_weights)
        print('==============finetune_biases=======================')
        print(finetune_biases)
        print('==============new_weights=======================')
        print(new_weights)
        print('==============new_biases=======================')
        print(new_biases)
    
        self.F_loss = self.Gregloss + self.G_loss * dann_coeff + self.metricloss * mtr_coeff

        op1=tf.train.MomentumOptimizer(learning_rate*0.1,0.9)
        op2=tf.train.MomentumOptimizer(learning_rate*0.2,0.9)
        op3 = tf.train.MomentumOptimizer(learning_rate*1.0,0.9)
        op4 = tf.train.MomentumOptimizer(learning_rate*2.0,0.9)

        wo_cyc_grad1 = tf.gradients(self.F_loss,finetune_weights)
        wo_cyc_grad2 = tf.gradients(self.F_loss,finetune_biases)
        wo_cyc_grad3 = tf.gradients(self.F_loss,new_weights)
        wo_cyc_grad4 = tf.gradients(self.F_loss,new_biases)
        #wo_cyc_grad3,_ = tf.al_norm(wo_cyc_grad3,5.0)
        #wo_cyc_grad4,_ = tf.clip_by_global_norm(wo_cyc_grad4,5.0)
        cyc_sf_grad  = tf.gradients(self.cycleloss * global_step * self.FLAGS.cychyp * cyc_coeff, self.source_feature)
        cyc_tf_grad  = tf.gradients(self.cycleloss * global_step * self.FLAGS.cychyp * cyc_coeff, self.target_feature)
        sf_grad1     = tf.gradients(self.source_feature, finetune_weights, cyc_sf_grad)
        sf_grad2     = tf.gradients(self.source_feature, finetune_biases, cyc_sf_grad)
        sf_grad3     = tf.gradients(self.source_feature, new_weights, cyc_sf_grad)
        sf_grad4     = tf.gradients(self.source_feature, new_biases, cyc_sf_grad)
        #sf_grad3,_   = tf.clip_by_global_norm(sf_grad3,5.0)
        #sf_grad4,_   = tf.clip_by_global_norm(sf_grad4,5.0)
        cyc_sigma_grad = tf.gradients(self.cycleloss * global_step * self.FLAGS.cychyp , new_weights[sig_idx])
        #cyc_sigma_grad,_ = tf.clip_by_global_norm(cyc_sigma_grad,5.0)
        #cyc_ent_coeff_grad = tf.gradients(self.cycleloss * global_step * 10, new_weights[ent_coeff_idx])
        #print(cyc_ent_coeff_grad)

        weighted_grad = cyc_tf_grad[0] * tf_grad_weight
        tf_grad1     = tf.gradients(self.target_feature, finetune_weights, weighted_grad)
        tf_grad2     = tf.gradients(self.target_feature, finetune_biases, weighted_grad)
        tf_grad3     = tf.gradients(self.target_feature, new_weights, weighted_grad)
        tf_grad4     = tf.gradients(self.target_feature, new_biases, weighted_grad)
        #tf_grad3,_   = tf.clip_by_global_norm(tf_grad3,5.0)
        #tf_grad4,_   = tf.clip_by_global_norm(tf_grad4,5.0)

        selfcyc_tf_grad = tf.gradients(self.selfcycleloss * global_step * self.FLAGS.cychyp * cyc_coeff, self.target_feature)
        selfcyc_slf_grad = tf.gradients(self.selfcycleloss * global_step * self.FLAGS.cychyp * cyc_coeff, self.selfcyc_feature)
        selfcyc_sigma_grad = tf.gradients(self.selfcycleloss * global_step * self.FLAGS.cychyp, new_weights[sig_idx])
        selfcyc_weighted_tf_grad = selfcyc_tf_grad[0] * tf_selfcyc_grad_weight
        selfcyc_tf_grad1 = tf.gradients(self.target_feature, finetune_weights, selfcyc_weighted_tf_grad)
        selfcyc_tf_grad2 = tf.gradients(self.target_feature, finetune_biases, selfcyc_weighted_tf_grad)
        selfcyc_tf_grad3 = tf.gradients(self.target_feature, new_weights, selfcyc_weighted_tf_grad)
        selfcyc_tf_grad4 = tf.gradients(self.target_feature, new_biases, selfcyc_weighted_tf_grad)
        selfcyc_weighted_slf_grad = selfcyc_slf_grad[0] * slf_selfcyc_grad_weight
        selfcyc_slf_grad1 = tf.gradients(self.selfcyc_feature, finetune_weights, selfcyc_weighted_slf_grad)
        selfcyc_slf_grad2 = tf.gradients(self.selfcyc_feature, finetune_biases, selfcyc_weighted_slf_grad)
        selfcyc_slf_grad3 = tf.gradients(self.selfcyc_feature, new_weights, selfcyc_weighted_slf_grad)
        selfcyc_slf_grad4 = tf.gradients(self.selfcyc_feature, new_biases, selfcyc_weighted_slf_grad)
        
        grad1 = []
        grad2 = []
        for i in range(len(finetune_weights)):
            if self.FLAGS.selfcyc_mode == 'None':
                grad1.append(wo_cyc_grad1[i]+sf_grad1[i]+tf_grad1[i])
            else:
                grad1.append(wo_cyc_grad1[i]+sf_grad1[i]+tf_grad1[i]+selfcyc_tf_grad1[i]+selfcyc_slf_grad1[i])
        for i in range(len(finetune_biases)):
            if self.FLAGS.selfcyc_mode == 'None':
                grad2.append(wo_cyc_grad2[i]+sf_grad2[i]+tf_grad2[i])    
            else:
                grad2.append(wo_cyc_grad2[i]+sf_grad2[i]+tf_grad2[i]+selfcyc_tf_grad2[i]+selfcyc_slf_grad2[i])    
        grad3 = []
        grad4 = []
        for i in range(len(new_weights)):
            if sf_grad3[i] == None:
                if i == sig_idx:
                    if self.FLAGS.selfcyc_mode == 'None':
                        grad3.append(wo_cyc_grad3[i] + cyc_sigma_grad[0])
                    else:
                        grad3.append(wo_cyc_grad3[i] + cyc_sigma_grad[0]+selfcyc_sigma_grad[0])
                #elif i == ent_coeff_idx:
                #    grad3.append(wo_cyc_grad3[i] + cyc_ent_coeff_grad[0])
                else:
                    grad3.append(wo_cyc_grad3[i])
            else:
                if self.FLAGS.selfcyc_mode == 'None':
                    grad3.append(wo_cyc_grad3[i]+sf_grad3[i]+tf_grad3[i])
                else:
                    grad3.append(wo_cyc_grad3[i]+sf_grad3[i]+tf_grad3[i]+selfcyc_tf_grad3[i] + selfcyc_slf_grad3[i])
        for i in range(len(new_biases)):
            if sf_grad4[i] == None:
                grad4.append(wo_cyc_grad4[i])
            else:
                if self.FLAGS.selfcyc_mode == 'None':
                    grad4.append(wo_cyc_grad4[i]+sf_grad4[i]+tf_grad4[i])
                else:
                    grad4.append(wo_cyc_grad4[i]+sf_grad4[i]+tf_grad4[i]+selfcyc_tf_grad4[i] + selfcyc_slf_grad4[i])

        print(grad3)
        #grad3, _ = tf.clip_by_global_norm(grad3, 5.0)
        #grad4, _ = tf.clip_by_global_norm(grad4, 5.0)
        train_op1 = op1.apply_gradients(list(zip(grad1,finetune_weights)))
        train_op2 = op2.apply_gradients(list(zip(grad2,finetune_biases)))
        train_op3 = op3.apply_gradients(list(zip(grad3,new_weights)))
        train_op4 = op4.apply_gradients(list(zip(grad4,new_biases)))
        #train_op3=tf.train.MomentumOptimizer(learning_rate*1.0,0.9).minimize(self.F_loss, var_list=new_weights)
        #train_op4=tf.train.MomentumOptimizer(learning_rate*2.0,0.9).minimize(self.F_loss, var_list=new_biases)
        train_op=tf.group(train_op1,train_op2,train_op3,train_op4)
        self.s_cyc_grad = tf.gradients(self.cycleloss, self.source_feature)
        self.t_cyc_grad = tf.gradients(self.cycleloss, self.target_feature)
        self.s_dann_grad = tf.gradients(self.G_loss, self.source_feature)
        self.t_dann_grad = tf.gradients(self.G_loss, self.target_feature)
        self.s_mtr_grad = tf.gradients(self.metricloss, self.source_feature)
        self.t_mtr_grad = tf.gradients(self.metricloss, self.target_feature)
        self.s_pmtr_grad = tf.gradients(self.puremtrloss, self.source_feature)
        #self.t_pmtr_grad = tf.gradients(self.puremtrloss, self.target_feature)
        self.sigma_grad = cyc_sigma_grad
        return train_op

    def assign_prior_op(self,prior_mean,prior_cov,prior_w,mean_val,cov_val,w_val,sess):
        assign_mean = prior_mean.assign(mean_val)
        assign_cov  = prior_cov.assign(cov_val)
        w_val = np.log(w_val)
        assign_w    = prior_w.assign(w_val)
        sess.run(tf.group(assign_mean,assign_cov,assign_w))
        return

    def assign_op(self):
        self.embeddings = tf.Variable(tf.zeros([self.batch_size*2,self.featurelen],name='embedding_initial_tensor'),name='real_Embedding')
        self.assignment = self.embeddings.assign(self.concat_feature)

    def second_assign_op(self):
        self.second_assignment = self.embeddings.assign(self.second_phase_feature)

    def load_original_weights(self, session, skip_layers=[]):
        weights_dict = np.load('/home/yjy765/BigData_Result/bvlc_alexnet.npy', encoding='bytes').item()

        for op_name in weights_dict:
            # if op_name in skip_layers:
            #     continue

            if op_name == 'fc8' and self.num_classes != 1000:
                continue
            if self.target_cf:
                with tf.variable_scope('target_inference/'+op_name, reuse=True):
                    print('=============================OP_NAME  ========================================')
                    for data in weights_dict[op_name]:
                        if len(data.shape) == 1:
                            var = tf.get_variable('biases')
                            print(op_name,var)
                            session.run(var.assign(data))
                        else:
                            var = tf.get_variable('weights')
                            print(op_name,var)
                            session.run(var.assign(data))
            else:
                with tf.variable_scope('reuse_inference/'+op_name, reuse=True):
                    print('=============================OP_NAME  ========================================')
                    for data in weights_dict[op_name]:
                        if len(data.shape) == 1:
                            var = tf.get_variable('biases')
                            print(op_name,var)
                            session.run(var.assign(data))
                        else:
                            var = tf.get_variable('weights')
                            print(op_name,var)
                            session.run(var.assign(data))

    def second_loss(self,x,xt,y,lamb,global_step=None):
        # yt : predicted label Not groud truth
        with tf.variable_scope('reuse_inference') as scope:
            scope.reuse_variables()
            self.inference(x,training=True)
            labeled_feature= self.feature
            scope.reuse_variables()
            self.inference(xt,training=True)
            unlabeled_feature=self.feature

        self.labeled_feature   = labeled_feature
        self.unlabeled_feature = unlabeled_feature
        self.second_phase_feature=tf.concat([labeled_feature,unlabeled_feature],0)    

        #### GET UNNORMALIZED SIMILARITY MATRIX W ####
        #### GET DISTANCE MATRIX M FIRST AND THEN CONVERT IT THROUGH GAUSSIAN ####
        if self.distance_form == 'euclidean':
            s_fb1 = tf.expand_dims(tf.transpose(labeled_feature,[1,0]),2)
            s_f1b = tf.transpose(s_fb1,[0,2,1])
            t_fb1 = tf.expand_dims(tf.transpose(unlabeled_feature,[1,0]),2)
            t_f1b = tf.transpose(t_fb1,[0,2,1])
            ## SS ##
            M_SS = tf.square(s_fb1 - s_f1b) + eps_M
            M_SS = tf.sqrt(tf.reduce_mean(M_SS,0))
            #W_SS 
            sigma_tile = tf.tile(tf.expand_dims(tf.expand_dims(self.sigma,1),2),[1,self.batch_size,self.batch_size])
            M_SS = tf.sqrt(tf.reduce_mean(M_SS*tf.square(sigma_tile),0))
            W_SS = tf.exp(-tf.square(M_SS)/2) 
            W_SS = W_SS - tf.diag(tf.diag_part(W_SS))

            ## ST & TS ##
            M_ST = tf.square(s_fb1 - t_f1b)
            M_ST = tf.sqrt(tf.reduce_mean(M_ST*tf.square(sigma_tile),0))
            W_ST = tf.exp(-tf.square(M_ST)/2)
            W_TS = tf.transpose(W_ST,[1,0])
            ## TT ##
            M_TT = tf.square(t_fb1 - t_f1b) + eps_M
            M_TT = tf.sqrt(tf.reduce_mean(M_TT*tf.square(sigma_tile),0))
            W_TT = tf.exp(-tf.square(M_TT)/2)
            W_TT = W_TT - tf.diag(tf.diag_part(W_TT))
        elif self.distance_form == 'L1':
            s_fb1 = tf.expand_dims(tf.transpose(labeled_feature,[1,0]),2)
            s_f1b = tf.transpose(s_fb1,[0,2,1])
            t_fb1 = tf.expand_dims(tf.transpose(unlabeled_feature,[1,0]),2)
            t_f1b = tf.transpose(t_fb1,[0,2,1])
            ## SS ##
            M_SS = tf.abs(s_fb1 - s_f1b)
            M_SS = tf.reduce_mean(M_SS,0)
            W_SS = tf.exp(-tf.square(M_SS)/2/self.sigma/self.sigma)
            W_SS = W_SS - tf.diag(tf.diag_part(W_SS))
            ## ST & TS ##
            M_ST = tf.abs(s_fb1 - t_f1b)
            M_ST = tf.reduce_mean(M_ST,0)
            W_ST = tf.exp(-tf.square(M_ST)/2/self.sigma/self.sigma)
            W_TS = tf.transpose(W_ST,[1,0])
            ## TT ##
            M_TT = tf.abs(t_fb1 - t_f1b)
            M_TT = tf.reduce_mean(M_TT,0)
            W_TT = tf.exp(-tf.square(M_TT)/2/self.sigma/self.sigma)
            W_TT = W_TT - tf.diag(tf.diag_part(W_TT))
        elif self.distance_form == 'correlation':
            pass
        elif self.distance_form == 'log_correlation':
            pass
        elif self.distance_form == 'inner_product':
            #### EXCEPTION ####
            #### GET SIMILARITY WITHOUT DISTANCE MATRIX ####
            s_fb1 = tf.expand_dims(tf.transpose(labeled_feature,[1,0]),2)
            s_f1b = tf.transpose(s_fb1,[0,2,1])
            t_fb1 = tf.expand_dims(tf.transpose(unlabeled_feature,[1,0]),2)
            t_f1b = tf.transpose(t_fb1,[0,2,1])
            ## SS ##
            W_SS = s_fb1 * s_f1b
            W_SS = tf.reduce_sum(W_SS,0)
            ## ST & TS ##
            W_ST = s_fb1 * t_f1b
            W_ST = tf.reduce_sum(W_ST,0)
            W_TS = tf.transpose(W_ST,[1,0])
            ## TT ##
            W_TT = t_fb1 * t_f1b
            W_TT = tf.reduce_sum(W_TT,0)
        else:
            raise NotImplementedError('1)euclidean 2)L1 3)correlation 4)log_correlation 5)inner_product')
                

        #### LABEL PROPAGATION ####
        if self.LP_or_LS == 'LP':
            yt_hat = LP_closed([W_TS,W_TT],y)
            y_hat  = LP_closed([W_ST,W_SS],yt_hat)
        elif self.LP_or_LS == 'LS':
            yt_hat = LS_closed([W_SS,W_ST,W_TS,W_TT],y)
            y_hat  = LS_closed([W_TT,W_TS,W_ST,W_SS],yt_hat)
        else:
            raise NotImplementedError('LP_or_LS should be among 1) LP or 2) LS')

        self.second_cycleloss = tf.reduce_mean(tf.reduce_sum(tf.abs(y-y_hat),1))
        
        ### f norm ###
        self.second_metricloss = lamb * tf.reduce_mean(tf.reduce_mean(labeled_feature * labeled_feature + unlabeled_feature * unlabeled_feature,1)) / 30.0
    
        tf.summary.scalar('second_metricloss',self.second_metricloss)
        tf.summary.scalar('second_cycleloss',self.second_cycleloss)

        return y_hat, yt_hat

    def second_optimize(self, learning_rate, train_layers,tf_grad_weight,cyc_coeff,mtr_coeff):
        print('++++++++++++++++++++++Train layers++++++++++++++++++++++')
        print(train_layers)
        print([(v.name,len(v.name.split('/'))) for v in tf.trainable_variables()])
        var_list = [v for v in tf.trainable_variables() if v.name.split('/')[1] in train_layers+['fc9','theta']]
        finetune_list=[v for v in var_list if v.name.split('/')[1] in ['conv1','conv2','conv3','conv4','conv5','fc6','fc7']]
        new_list=[v for v in var_list if v.name.split('/')[1] in ['fc8','fc9','theta']]
        self.Gregloss=0.0005*tf.reduce_mean([tf.nn.l2_loss(x) for x in var_list if 'weights' in x.name])
    
        finetune_weights=[v for v in finetune_list if 'weights' in v.name]
        finetune_biases=[v for v in finetune_list if 'biases' in v.name]
        new_weights=[v for v in new_list if 'weights' in v.name]
        new_biases=[v for v in new_list if 'biases' in v.name]

        for i in range(len(new_weights)):
            if new_weights[i].name.split('/')[1] == 'theta':
                if new_weights[i].name.split('/')[2] == 'entropy_coeff':
                    ent_coeff_idx = i
                elif new_weights[i].name.split('/')[2] == 'sigma':
                    sig_idx = i
                else:
                    sig_idx = i

   
        self.second_loss = 0* self.Gregloss + 0 * self.second_metricloss * mtr_coeff

        train_op1=tf.train.MomentumOptimizer(learning_rate*0.1,0.9).minimize(self.second_loss, var_list=finetune_weights)
        train_op2=tf.train.MomentumOptimizer(learning_rate*0.2,0.9).minimize(self.second_loss, var_list=finetune_biases)

        op3 = tf.train.MomentumOptimizer(learning_rate*1.0,0.9)
        op4 = tf.train.MomentumOptimizer(learning_rate*2.0,0.9)
        wo_cyc_grad3 = tf.gradients(self.second_loss,new_weights)
        wo_cyc_grad4 = tf.gradients(self.second_loss,new_biases)
        cyc_sf_grad  = tf.gradients(self.second_cycleloss * 10 * cyc_coeff, self.labeled_feature)
        cyc_tf_grad  = tf.gradients(self.second_cycleloss * 10 * cyc_coeff, self.unlabeled_feature)
        sf_grad3     = tf.gradients(self.labeled_feature, new_weights, cyc_sf_grad)
        sf_grad4     = tf.gradients(self.labeled_feature, new_biases, cyc_sf_grad)
        cyc_sigma_grad = tf.gradients(self.second_cycleloss * 10 , new_weights[sig_idx])

        weighted_grad = cyc_tf_grad[0] * tf_grad_weight
        tf_grad3     = tf.gradients(self.unlabeled_feature, new_weights, weighted_grad)
        tf_grad4     = tf.gradients(self.unlabeled_feature, new_biases, weighted_grad)
        grad3 = []
        grad4 = []
        for i in range(len(new_weights)):
            if sf_grad3[i] == None:
                if i == sig_idx:
                    grad3.append(wo_cyc_grad3[i] + cyc_sigma_grad[0])
                else:
                    grad3.append(wo_cyc_grad3[i])
            else:
                grad3.append(wo_cyc_grad3[i]+sf_grad3[i]+tf_grad3[i])
        for i in range(len(new_biases)):
            if sf_grad4[i] == None:
                grad4.append(wo_cyc_grad4[i])
            else:
                grad4.append(wo_cyc_grad4[i]+sf_grad4[i]+tf_grad4[i])

        train_op3 = op3.apply_gradients(list(zip(grad3,new_weights)))
        train_op4 = op4.apply_gradients(list(zip(grad4,new_biases)))
        train_op=tf.group(train_op1,train_op2,train_op3,train_op4)

        self.second_s_cyc_grad = tf.gradients(self.second_cycleloss, self.labeled_feature)
        self.second_t_cyc_grad = tf.gradients(self.second_cycleloss, self.unlabeled_feature)
        self.second_s_mtr_grad = tf.gradients(self.second_metricloss, self.labeled_feature)
        self.second_t_mtr_grad = tf.gradients(self.second_metricloss, self.unlabeled_feature)

        return train_op



"""
Helper methods
"""
def conv(x, filter_height, filter_width, num_filters, stride_y, stride_x, name, padding='SAME', groups=1):
    input_channels = int(x.get_shape()[-1])
    convolve = lambda i, k: tf.nn.conv2d(i, k, strides=[1, stride_y, stride_x, 1], padding=padding)

    with tf.variable_scope(name) as scope:
        weights = tf.get_variable('weights', shape=[filter_height, filter_width, input_channels/groups, num_filters])
        biases = tf.get_variable('biases', shape=[num_filters])

        if groups == 1:
            conv = convolve(x, weights)
        else:
            input_groups = tf.split(axis=3, num_or_size_splits=groups, value=x)
            weight_groups = tf.split(axis=3, num_or_size_splits=groups, value=weights)
            output_groups = [convolve(i, k) for i,k in zip(input_groups, weight_groups)]
            conv = tf.concat(axis=3, values=output_groups)

        bias = tf.reshape(tf.nn.bias_add(conv, biases), [-1]+conv.get_shape().as_list()[1:])
        relu = tf.nn.relu(bias, name=scope.name)
        return relu
def D(x):
    with tf.variable_scope('D'):
        num_units_in=int(x.get_shape()[-1])
        num_units_out=1
        weights = tf.get_variable('weights',initializer=tf.truncated_normal([num_units_in,1024],stddev=0.01))
        biases = tf.get_variable('biases', shape=[1024], initializer=tf.zeros_initializer())
        hx=(tf.matmul(x,weights)+biases)
        ax=tf.nn.dropout(tf.nn.relu(hx),0.5)
            

        weights2 = tf.get_variable('weights2',initializer=tf.truncated_normal([1024,1024],stddev=0.01))
        biases2 = tf.get_variable('biases2', shape=[1024], initializer=tf.zeros_initializer())
        hx2=(tf.matmul(ax,weights2)+biases2)
        ax2=tf.nn.dropout(tf.nn.relu(hx2),0.5)
    
        weights3 = tf.get_variable('weights3', initializer=tf.truncated_normal([1024,num_units_out],stddev=0.3))
        biases3 = tf.get_variable('biases3', shape=[num_units_out], initializer=tf.zeros_initializer())
        hx3=tf.matmul(ax2,weights3)+biases3
        return hx3,tf.nn.sigmoid(hx3)

def fc(x, num_in, num_out, name, relu=True,stddev=0.01):
    with tf.variable_scope(name) as scope:
        weights = tf.get_variable('weights', initializer=tf.truncated_normal([num_in,num_out],stddev=stddev))
        biases = tf.get_variable('biases',initializer=tf.constant(0.1,shape=[num_out]))
        act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)

        if relu == True:
            relu = tf.nn.relu(act)
            return relu
        else:
            return act
def leaky_relu(x, alpha=0.2):
    return tf.maximum(tf.minimum(0.0, alpha * x), x)

def outer(a,b):
        a=tf.reshape(a,[-1,a.get_shape()[-1],1])
        b=tf.reshape(b,[-1,1,b.get_shape()[-1]])
        c=a*b
        return tf.contrib.layers.flatten(c)

def max_pool(x, filter_height, filter_width, stride_y, stride_x, name, padding='SAME'):
    return tf.nn.max_pool(x, ksize=[1, filter_height, filter_width, 1], strides = [1, stride_y, stride_x, 1],
                          padding = padding, name=name)

def lrn(x, radius, alpha, beta, name, bias=1.0):
    return tf.nn.local_response_normalization(x, depth_radius=radius, alpha=alpha, beta=beta, bias=bias, name=name)

def dropout(x, keep_prob):
    return tf.nn.dropout(x, keep_prob)
