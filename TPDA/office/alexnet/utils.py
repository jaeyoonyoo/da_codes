import numpy as np
import tensorflow as tf
import math

eps = 1e-6

def bond_energy(r, k1=1, k2=1):
    return -k1/(r+eps) + k2/(r+eps)/(r+eps)

def get_grad_weight(yt_hat, featurelen):
    if np.isnan(yt_hat).any():
        print('nannnnnnnnnnnn')
    class_num = yt_hat.shape[1]
    ent = np.sum(-yt_hat * np.log(yt_hat+eps),1)
    weight = (ent/1.5 * np.exp(-ent/1.5+1))
    weight = np.tile(np.expand_dims(weight,1),[1,featurelen])
    #weight = np.ones_like(weight)
    return weight

def get_lognormal_momentum(r_array,var_wannab=1.):
    first_mom = np.mean(r_array)
    sigma_sq = math.log(1+var_wannab/first_mom**2)
    sigma_sq = min(sigma_sq,1)
    second_mom = np.exp(sigma_sq) * first_mom**2 
    return np.asarray([first_mom]), np.asarray([second_mom])
        

def get_entropy(yt_hat, featurelen):
    if np.isnan(yt_hat).any():
        print('nannnnnnnnnnnn')
    class_num = yt_hat.shape[1]
    ent = np.sum(-yt_hat * np.log(yt_hat+eps),1,keepdims=True)
    ent = np.tile(ent,[1,featurelen])
    return ent

#### THINK HOW TO EXPLOIT THE PREDICTED TARGET LABEL ####
def LP_closed(trans_list,l):
    A_ul, A_uu = trans_list
    A_ul = A_ul + eps
    A_uu = A_uu + eps
    node_num_u, node_num_l = A_ul.get_shape().as_list()
    if node_num_u == None:
        node_num = node_num_l
    else:
        node_num = node_num_u
    A = tf.concat([A_ul,A_uu],1)
    D = tf.tile(tf.reduce_sum(A,1,keepdims=True),[1,node_num])
    P_ul = A_ul / D
    P_uu = A_uu / D
    return tf.matmul(tf.matrix_inverse(tf.eye(node_num) - P_uu), tf.matmul(P_ul,l))
def LS_closed(trans_list,l):
    raise NotImplementedError('not yet LS closed form')
def LP_iter(trans_list,l,u):
    A_ul = trans_list[0]
    A_uu = trans_list[1]
    A_ul = A_ul + eps
    A_uu = A_uu + eps
    node_num_u, node_num_l = A_ul.get_shape().as_list()
    if node_num_u == None:
        node_num = node_num_l
    else:
        node_num = node_num_u
    A = tf.concat([A_ul,A_uu],1)
    D = tf.tile(tf.reduce_sum(A,1,keepdims=True),[1,node_num])
    P_ul = A_ul / D
    P_uu = A_uu / D
    return tf.matmul(P_ul,l) + tf.matmul(P_uu, u)
def LS_iter(trans_list,l,u,y0,alpha=0.2):
    A_ll, A_lu, A_ul, A_uu = trans_list
    A_ll += eps
    A_lu += eps
    A_ul += eps
    A_uu += eps
    node_num = A_ul.get_shape().as_list()[1]
    A_l = tf.concat([A_ll,A_lu],1)
    A_u = tf.concat([A_ul,A_uu],1)
    A   = tf.concat([A_l,A_u],0)
    D_inv_sqrt = tf.diag(tf.reciprocal(tf.sqrt(tf.reduce_sum(A,1)+eps)))
    S   = D_inv_sqrt * A * D_inv_sqrt
    y = tf.concat([l,u],0)
    y = alpha * S * y + (1 - alpha) * y0
    return y[:node_num], y[node_num:]

def Quadratic(f,L):
    F = tf.diag_part(tf.matmul(tf.matmul(tf.transpose(f,[1,0]),L),f)/2)
    return tf.reduce_mean(F)

def CE(f_pred, y):
    return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=f_pred, labels=y))

def Source_LP(y, W_SS):
    W_SS += eps
    node_num = W_SS.get_shape().as_list()[1]
    D = tf.tile(tf.reduce_sum(W_SS,1,keepdims=True),[1,node_num])
    P_SS = W_SS / D
    return tf.reduce_mean(tf.reduce_sum(tf.abs(tf.matmul(P_SS, y) - y),1))

def conv_lap(y):
    y_fb1 = np.expand_dims(np.transpose(y,[1,0]),2)
    y_f1b = np.transpose(y_fb1,[0,2,1])
    y_fbb = y_fb1 * y_f1b
    M_eq  = np.sum(y_fbb,0)
    M     = -2 * M_eq + 1
    D     = np.diag(np.sum(M,1))
    L     = D - M
    return L

def split_high_score(self_label_array, true_label_array, tx_f, tx_s, idx_f, idx_s, batch_size):
    ### choose the half high confidence index and data
    score_order_idx = np.argsort(np.concatenate([np.max(self_label_array[idx_f,:],1),np.max(self_label_array[idx_s,:],1)]))[::-1]
    score_order_idx = score_order_idx[:batch_size]
    idx_f = np.array(idx_f)
    idx_s = np.array(idx_s)
    labeled_f_idx = idx_f[score_order_idx[score_order_idx<batch_size]]
    unlabeled_f_idx = np.setdiff1d(idx_f,labeled_f_idx)
    labeled_s_idx = idx_s[score_order_idx[score_order_idx>=batch_size]-batch_size]
    unlabeled_s_idx = np.setdiff1d(idx_s,labeled_s_idx)
    batch_labeled_f_idx = score_order_idx[score_order_idx<batch_size]
    batch_unlabeled_f_idx = np.setdiff1d(np.arange(batch_size),batch_labeled_f_idx)
    batch_labeled_s_idx = score_order_idx[score_order_idx>=batch_size] - batch_size
    batch_unlabeled_s_idx = np.setdiff1d(np.arange(batch_size),batch_labeled_s_idx)
    labeled_x = np.concatenate([tx_f[batch_labeled_f_idx,:],tx_s[batch_labeled_s_idx,:]])
    labeled_y = np.concatenate([self_label_array[labeled_f_idx,:],self_label_array[labeled_s_idx,:]])
    unlabeled_x = np.concatenate([tx_f[batch_unlabeled_f_idx,:],tx_s[batch_unlabeled_s_idx,:]])
    unlabeled_y = np.concatenate([self_label_array[unlabeled_f_idx,:],self_label_array[unlabeled_s_idx,:]])
    gt_labeled_y = np.concatenate([true_label_array[labeled_f_idx,:],true_label_array[labeled_s_idx,:]])
    gt_unlabeled_y = np.concatenate([true_label_array[unlabeled_f_idx,:],true_label_array[unlabeled_s_idx,:]])
    return labeled_x, labeled_y, unlabeled_x, unlabeled_y, gt_labeled_y, gt_unlabeled_y

def split_random(self_label_array, true_label_array, tx_f, tx_s, idx_f, idx_s, batch_size):
    return tx_f, self_label_array[idx_f,:], tx_s, self_label_array[idx_s,:], true_label_array[idx_f,:], true_label_array[idx_s,:]


def get_graph_weight(distance_form, source_feature, target_feature, sigma, batch_size):
    eps_M = 1e-6
    #### GET UNNORMALIZED SIMILARITY MATRIX W ####
    #### GET DISTANCE MATRIX M FIRST AND THEN CONVERT IT THROUGH GAUSSIAN ####
    if distance_form == 'euclidean':
        s_fb1 = tf.expand_dims(tf.transpose(source_feature,[1,0]),2)
        s_f1b = tf.transpose(s_fb1,[0,2,1])
        t_fb1 = tf.expand_dims(tf.transpose(target_feature,[1,0]),2)
        t_f1b = tf.transpose(t_fb1,[0,2,1])
        ## SS ##
        M_SS = tf.square(s_fb1 - s_f1b) + eps_M
        M_SS = tf.sqrt(tf.reduce_mean(M_SS,0))
        #W_SS 
        sigma_tile = tf.tile(tf.expand_dims(tf.expand_dims(sigma,1),2),[1,batch_size,batch_size])
        M_SS = tf.sqrt(tf.reduce_mean(M_SS*tf.square(sigma_tile),0))
        W_SS = tf.exp(-tf.square(M_SS)/2) 
        W_SS = W_SS - tf.diag(tf.diag_part(W_SS))

        ## ST & TS ##
        M_ST = tf.square(s_fb1 - t_f1b)
        M_ST = tf.sqrt(tf.reduce_mean(M_ST*tf.square(sigma_tile),0))
        W_ST = tf.exp(-tf.square(M_ST)/2)
        W_TS = tf.transpose(W_ST,[1,0])
        ## TT ##
        M_TT = tf.square(t_fb1 - t_f1b) + eps_M
        M_TT = tf.sqrt(tf.reduce_mean(M_TT*tf.square(sigma_tile),0))
        W_TT = tf.exp(-tf.square(M_TT)/2)
        W_TT = W_TT - tf.diag(tf.diag_part(W_TT))
    elif distance_form == 'L1':
        s_fb1 = tf.expand_dims(tf.transpose(source_feature,[1,0]),2)
        s_f1b = tf.transpose(s_fb1,[0,2,1])
        t_fb1 = tf.expand_dims(tf.transpose(target_feature,[1,0]),2)
        t_f1b = tf.transpose(t_fb1,[0,2,1])
        ## SS ##
        M_SS = tf.abs(s_fb1 - s_f1b)
        M_SS = tf.reduce_mean(M_SS,0)
        W_SS = tf.exp(-tf.square(M_SS)/2/sigma/sigma)
        W_SS = W_SS - tf.diag(tf.diag_part(W_SS))
        ## ST & TS ##
        M_ST = tf.abs(s_fb1 - t_f1b)
        M_ST = tf.reduce_mean(M_ST,0)
        W_ST = tf.exp(-tf.square(M_ST)/2/sigma/sigma)
        W_TS = tf.transpose(W_ST,[1,0])
        ## TT ##
        M_TT = tf.abs(t_fb1 - t_f1b)
        M_TT = tf.reduce_mean(M_TT,0)
        W_TT = tf.exp(-tf.square(M_TT)/2/sigma/sigma)
        W_TT = W_TT - tf.diag(tf.diag_part(W_TT))
    elif distance_form == 'correlation':
        pass
    elif distance_form == 'log_correlation':
        pass
    elif distance_form == 'inner_product':
        #### EXCEPTION ####
        #### GET SIMILARITY WITHOUT DISTANCE MATRIX ####
        s_fb1 = tf.expand_dims(tf.transpose(source_feature,[1,0]),2)
        s_f1b = tf.transpose(s_fb1,[0,2,1])
        t_fb1 = tf.expand_dims(tf.transpose(target_feature,[1,0]),2)
        t_f1b = tf.transpose(t_fb1,[0,2,1])
        ## SS ##
        W_SS = s_fb1 * s_f1b
        W_SS = tf.reduce_sum(W_SS,0)
        ## ST & TS ##
        W_ST = s_fb1 * t_f1b
        W_ST = tf.reduce_sum(W_ST,0)
        W_TS = tf.transpose(W_ST,[1,0])
        ## TT ##
        W_TT = t_fb1 * t_f1b
        W_TT = tf.reduce_sum(W_TT,0)
    else:
        raise NotImplementedError('1)euclidean 2)L1 3)correlation 4)log_correlation 5)inner_product')
    return W_SS, W_ST, W_TS, W_TT
