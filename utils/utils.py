#### https://github.com/jindongwang/tf-dann/blob/master/utils.py
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import scipy.misc

def shuffle_aligned_list(data):
    """Shuffle arrays in a list by shuffling each array identically."""
    num = data[0].shape[0]
    p = np.random.permutation(num)
    return [d[p] for d in data]


def batch_generator(data, batch_size, shuffle=True):
    """Generate batches of data.
       Given a list of array-like objects, generate batches of a given
       size by yielding a list of array-like objects corresponding to the
       same slice of each input.
    """
    if shuffle:
        data = shuffle_aligned_list(data)
    batch_count = 0
    while True:
        if batch_count * batch_size + batch_size >= len(data[0]):
            batch_count = 0
            if shuffle:
                data = shuffle_aligned_list(data)
        start = batch_count * batch_size
        end = start + batch_size
        batch_count += 1
        start = (int)(start)
        end = (int)(end)
        yield [d[start:end] for d in data]

def plot_embedding(X, y, d, title=None):
    """Plot an embedding X with the class label y colored by the domain d."""
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)
    # Plot colors numbers
    plt.figure(figsize=(10,10))
    ax = plt.subplot(111)
    for i in range(X.shape[0]):
        # plot colored number
        plt.text(X[i, 0], X[i, 1], str(y[i]),
                 color=plt.cm.bwr(d[i] / 1.),
                 fontdict={'weight': 'bold', 'size': 9})
    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)
    plt.savefig(title+'.png')

def resize_images(image_arrays, size=(32, 32, 3)):
    # convert float type to integer 
    #image_arrays = (image_arrays * 255).astype('uint8')
    
    resized_image_arrays = np.zeros((image_arrays.shape[0],)+size)
    for i in range(image_arrays.shape[0]):
        image_array = image_arrays[i]
        #image = Image.fromarray(image_array)
        #resized_image = image.resize(size=size, resample=Image.ANTIALIAS)
        resized_image = scipy.misc.imresize(image_array,size[:2])
        resized_image_arrays[i] = resized_image
    
    return resized_image_arrays
