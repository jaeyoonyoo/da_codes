import tensorflow as tf
import numpy as np
from tensorflow.examples.tutorials.mnist import input_data
import os
import pickle
import scipy.io
import scipy.misc

data_dir = '/home/yjy765/BigData_Result'

def mnist(size=(28,28,3)):
    data_dir = '/home/yjy765/BigData_Result/mnist'

    mnist_dir = os.path.join(data_dir,'mnist')
    mnist = input_data.read_data_sets(mnist_dir, one_hot=True)
    ### BINARY AND RGB CHANNEL ###
    mnist_train = (mnist.train.images > 0).reshape(55000, 28, 28, 1).astype(np.uint8) * 255
    #mnist_train = mnist.train.images.reshape(55000, 28, 28, 1).astype(np.float32)
    mnist_train = np.concatenate([mnist_train, mnist_train, mnist_train], 3)
    mnist_test = (mnist.test.images > 0).reshape(10000, 28, 28, 1).astype(np.uint8) * 255
    #mnist_test = mnist.test.images.reshape(10000,28,28,1).astype(np.float32)
    mnist_test = np.concatenate([mnist_test, mnist_test, mnist_test], 3)

    if mnist_train.shape[1:] == size:
        pass
    else:
        mnist_train_copy = mnist_train
        mnist_train = np.zeros((mnist_train.shape[0],)+size,np.float32)
        for i in range(mnist_train.shape[0]):
            mnist_train[i] = scipy.misc.imresize(mnist_train_copy[i],size[:2])
        mnist_test_copy  = mnist_test
        mnist_test  = np.zeros((mnist_test.shape[0],)+size,np.float32)
        for i in range(mnist_test.shape[0]):
            mnist_test[i] = scipy.misc.imresize(mnist_test_copy[i],size[:2])

    mnist_mean = np.float32(mnist_train.mean((0,1,2)))
    mnist_std  = np.float32(mnist_train.std((0,1,2)))
    mnist_train = (mnist_train-mnist_mean)/mnist_std
    mnist_test  = (mnist_test-mnist_mean)/mnist_std

    return mnist_train, None, mnist_test, mnist_mean, mnist_std, mnist.train.labels, None, mnist.test.labels

def mnistm(size=(28,28,3)):
    mnistm_dir = os.path.join(data_dir,'mnist_m')
    mnistm = pickle.load(open(os.path.join(mnistm_dir,'mnistm_data.pkl'),'rb'))
    mnistm_train = mnistm['train'][0]
    mnistm_test = mnistm['test'][0]
    mnistm_valid = mnistm['valid'][0]

    if mnistm_train.shape[1:] == size:
        pass
    else:
        mnistm_train_copy = mnistm_train
        mnistm_train = np.zeros((mnistm_train.shape[0],)+size,np.float32)
        for i in range(mnistm_train.shape[0]):
            mnistm_train[i] = scipy.misc.imresize(mnistm_train_copy[i],size[:2])
        mnistm_test_copy  = mnistm_test
        mnistm_test  = np.zeros((mnistm_test.shape[0],)+size,np.float32)
        for i in range(mnistm_test.shape[0]):
            mnistm_test[i] = scipy.misc.imresize(mnistm_test_copy[i],size[:2])

    mnistm_mean = np.float32(mnistm_train.mean((0,1,2)))
    mnistm_std  = np.float32(mnistm_train.std((0,1,2)))
    mnistm_train = (mnistm_train - mnistm_mean)/mnistm_std
    mnistm_test  = (mnistm_test  - mnistm_mean)/mnistm_std
    mnistm_valid = (mnistm_valid - mnistm_mean)/mnistm_std

    return mnistm_train, mnistm_valid, mnistm_test, mnistm_mean, mnistm_std, mnistm['train'][1], mnistm['valid'][1], mnistm['test'][1]

def svhn(size=(32,32,3)):
    svhn_dir = os.path.join(data_dir,'svhn')
    svhn_train, svhn_train_label = load_svhn(svhn_dir,'train')
    svhn_test,  svhn_test_label  = load_svhn(svhn_dir,'test')

    if svhn_train.shape[1:] == size:
        pass
    else:
        svhn_train_copy = svhn_train
        svhn_train = np.zeros((svhn_train.shape[0],)+size,np.float32)
        for i in range(svhn_train.shape[0]):
            svhn_train[i] = scipy.misc.imresize(svhn_train_copy[i],size[:2])
        svhn_test_copy  = svhn_test
        svhn_test  = np.zeros((svhn_test.shape[0],)+size,np.float32)
        for i in range(svhn_test.shape[0]):
            svhn_test[i] = scipy.misc.imresize(svhn_test_copy[i],size[:2])

    svhn_mean = np.float32(svhn_train.mean((0,1,2)))
    svhn_std  = np.float32(svhn_train.std((0,1,2)))
    svhn_train= (svhn_train - svhn_mean)/svhn_std
    svhn_test = (svhn_test  - svhn_mean)/svhn_std

    return svhn_train, None, svhn_test, svhn_mean, svhn_std, svhn_train_label, None, svhn_test_label


def load_svhn(image_dir, split='train'):
    print ('Loading SVHN dataset.')
        
    image_file = 'train_32x32.mat' if split=='train' else 'test_32x32.mat'
            
    image_dir = os.path.join(image_dir, image_file)
    svhn = scipy.io.loadmat(image_dir)
    images = np.transpose(svhn['X'], [3, 0, 1, 2]) / 127.5 - 1
    #~ images= resize_images(images)
    labels = svhn['y'].reshape(-1)
    labels[np.where(labels==10)] = 0
    onehot_labels = np.zeros((labels.shape[0],10)).astype(np.uint8)
    onehot_labels[range(labels.shape[0]),list(labels)] = 1
    #return images, onehot_labels
    return images, onehot_labels
