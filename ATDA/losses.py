import tensorflow as tf

def adversarial_loss(disc_source,disc_target):
    print('disc source shape')
    print(disc_source.get_shape())
    print(disc_source[:,0].get_shape())
    d_loss = -tf.reduce_mean(tf.log(disc_source[:,0]) + tf.log(1-disc_target[:,0]))
    g_loss = -d_loss
    return g_loss, d_loss

def classification_loss(logit, label):
    return tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logit, labels=label))
