import tensorflow as tf
import modules
import losses

class model():
    def __init__(self, source_data, target_data, args):
        self.source_data   = source_data
        self.target_data   = target_data
        self.shared_net    = modules.shared_net(source_data, target_data, args)
        self.classifier_1  = modules.task(source_data, target_data, args)
        self.classifier_2  = modules.task(source_data, target_data, args) 
        self.classifier_t  = modules.task(source_data, target_data, args)
        

    def __call__(self, X, is_training, keep_prob):
        with tf.name_scope('shared_net'):
            try:
                feature_func = getattr(self.shared_net, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no shared network for source : %s target :%s' % (self.source_data, self.target_data))
            self.feature = feature_func(X, is_training, keep_prob, name='shared_net', reuse=False)
        with tf.name_scope('classifier_1'):
            try:
                pred1_func = getattr(self.classifier_1, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no classifier for source : %s target : %s' % (self.source_data, self.target_data))
            self.pred1_logit, self.pred1_W = pred1_func(self.feature, is_training, keep_prob, is_target=False, name='classifier_1', reuse=False)
        with tf.name_scope('classifier_2'):
            try:
                pred2_func = getattr(self.classifier_2, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no classifier for source : %s target : %s ' % (self.source_data, self.target_data))
            self.pred2_logit, self.pred2_W = pred2_func(self.feature, is_training, keep_prob, is_target=False, name='classifier_2', reuse=False)
        with tf.name_scope('classifier_t'):
            try:
                predt_func = getattr(self.classifier_t, self.source_data + '2' + self.target_data)
            except:
                raise AttributeError('no classifier for source : %s target : %s ' % (self.source_data, self.target_data))
            self.predt_logit = predt_func(self.feature, is_training, keep_prob, is_target=True, name='classifier_t', reuse=False)

    def create_objective(self, label):
        with tf.name_scope('classifier_1'):
            self.loss_1 = losses.classification_loss(self.pred1_logit, label)
        with tf.name_scope('classifier_2'):
            self.loss_2 = losses.classification_loss(self.pred2_logit, label)
        with tf.name_scope('classifier_t'):
            self.loss_t = losses.classification_loss(self.predt_logit, label)
        orthogonal_W = tf.matmul(self.pred1_W,self.pred2_W,transpose_a = True)
        orthogonal_W = tf.abs(orthogonal_W)
        self.orthogonal_loss = (tf.reduce_sum(orthogonal_W) - tf.trace(orthogonal_W))/2048
