import tensorflow as tf
import numpy as np
from atda_model import model
import pickle
from tensorflow.examples.tutorials.mnist import input_data
import os
import sys
sys.path.append('../utils')
import op
from utils import *
from sklearn.manifold import TSNE
import get_data

def _get_trainable_vars(scope):
    is_trainable = lambda x : x in tf.trainable_variables()

    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope)
    var_list = list(filter(is_trainable, var_list))
    return var_list

def _get_optimizer(name, optimizer, loss, clip_norm=5.0):
    if name == '':
        var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    else:
        var_list = _get_trainable_vars(name)
    print(name)
    print('--------')
    print(var_list)
    print('========')
    grad, var = zip(*optimizer.compute_gradients(loss, var_list=var_list))
    gradients = [gradient if gradient is None else tf.clip_by_norm(gradient, clip_norm) for gradient in grad]
    optim = optimizer.apply_gradients(zip(gradients,var))
    #optim = optimizer.apply_gradients(zip(grad,var))
    return optim


def train(sess, args):
    dataset = [['mnist','mnistm'],['mnistm','mnist'],['mnist','svhn'],['svhn','mnist']]
    datasize = [[(28,28,3),(28,28,3)],[(28,28,3),(28,28,3)],[(32,32,3),(32,32,3)],[(32,32,3),(32,32,3)]]
    source_data = dataset[args.dataset][0]
    target_data = dataset[args.dataset][1]
    source_size = datasize[args.dataset][0]
    target_size = datasize[args.dataset][1]
    atda_model  = model(source_data, target_data, args)

    get_source = getattr(get_data, source_data)
    get_target = getattr(get_data, target_data)
    
    source_train, source_valid, source_test, source_mean, source_std, source_train_label, source_valid_label, source_test_label  = get_source(size=source_size)
    target_train, target_valid, target_test, target_mean, target_std, target_train_label, target_valid_label, target_test_label  = get_target(size=target_size)    

    print(target_test.shape)
    print(target_test_label.shape)

    #if source_train.shape[1:] != source_size:
    #    source_train =  resize_images(source_train, source_size)
    #    source_test  =  resize_images(source_test,  source_size)
    #if target_train.shape[1:] != target_size:
    #    target_train =  resize_images(target_train, target_size)
    #    target_test  =  resize_images(target_test,  target_size)


    source_batch = tf.placeholder(tf.float32, (None,)+source_size)
    source_label = tf.placeholder(tf.int8, (None,10))
    target_batch = tf.placeholder(tf.float32, (None,)+target_size)
    is_training  = tf.placeholder(tf.bool,[])
    keep_prob    = tf.placeholder(tf.float32)

    atda_model(source_batch, is_training, keep_prob)
    atda_model.create_objective(source_label)

    pred12_loss = atda_model.loss_1 + atda_model.loss_2  + args.lamb * atda_model.orthogonal_loss
    predt_loss = atda_model.loss_t

    pred1_prob  = tf.nn.softmax(atda_model.pred1_logit)
    pred2_prob  = tf.nn.softmax(atda_model.pred2_logit)

    #Eval
    #domain_correct = tf.concat([tf.equal(tf.constant(np.zeros(args.batch_size),tf.int64), tf.argmax(dann_model.disc_source,1)) , tf.equal(tf.constant(np.ones(args.batch_size),tf.int64),tf.argmax(dann_model.disc_target,1))],0)
    #print(domain_correct.get_shape())
    #domain_acc     = tf.reduce_mean(tf.cast(domain_correct, tf.float32))
    task1_correct   = tf.equal(tf.argmax(source_label,1), tf.argmax(atda_model.pred1_logit,1))
    task1_acc       = tf.reduce_mean(tf.cast(task1_correct, tf.float32))
    task2_correct   = tf.equal(tf.argmax(source_label,1), tf.argmax(atda_model.pred2_logit,1))
    task2_acc       = tf.reduce_mean(tf.cast(task2_correct, tf.float32))
    taskt_correct   = tf.equal(tf.argmax(source_label,1), tf.argmax(atda_model.predt_logit,1))
    taskt_acc       = tf.reduce_mean(tf.cast(taskt_correct, tf.float32))
    
    with tf.name_scope('optimizer'):
        update_ops    = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        learning_rate = tf.placeholder(tf.float32,[])
        with tf.control_dependencies(update_ops):
            supervised_op = tf.train.MomentumOptimizer(learning_rate, 0.9).minimize(predt_loss)
            pred12_optimizer   = tf.train.MomentumOptimizer(learning_rate, 0.9).minimize(pred12_loss)
        #d_optimizer   = tf.train.MomentumOptimizer(learning_rate, 0.9)
        #c_optimizer   = tf.train.MomentumOptimizer(learning_rate, 0.9)
        #g_op          = _get_optimizer('feature_generator',g_optimizer,generator_loss)
        #d_op          = _get_optimizer('domain_disc',d_optimizer,discriminator_loss)
        #c_op          = _get_optimizer('task_classifier',c_optimizer,classifier_loss)
    var_list = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    print(var_list)

    saver = tf.train.Saver(max_to_keep=5)
    sess.run(tf.global_variables_initializer())

    target_train = np.float32(target_train)



    #import matplotlib.pyplot as plt
    #import scipy.misc
    #import time
    #s_im, s_lb = gen_source_batch.__next__()
    #t_im, t_lb = gen_target_batch.__next__()
    #for i in range(3):
    #    print(s_lb[i])
    #    scipy.misc.imshow(s_im[i])
    #time.sleep(10)
    #print(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
    #print('===============================')
    print(target_train.dtype)
    print(np.max(target_train))
    print(np.min(target_train))
    print(source_train.dtype)
    print(np.max(source_train))
    print(np.min(source_train))

    for t in range(30):
        if t==0:
            gen_source_batch = batch_generator([source_train, source_train_label], args.batch_size)
        else:
            new_data          = np.concatenate([source_train,labeled_train[:labeled_num]],0)
            print(new_data.shape)
            new_data_label    = np.concatenate([source_train_label,labeled_train_label[:labeled_num]],0)
            print(new_data_label.shape)
            gen_source_batch  = batch_generator([new_data, new_data_label], args.batch_size)
            gen_labeled_batch = batch_generator([labeled_train[:labeled_num], labeled_train_label[:labeled_num]], args.batch_size)
        gen_target_batch = batch_generator([target_train, target_train_label], args.batch_size)

        num_steps = 4000
        for idx in range(num_steps):
            p = float(idx) / num_steps
            l = 2. / (1. + np.exp(-10. * p)) - 1
            d_lr = 0.001 / (1. + 10 * p)**0.75
            if t == 0:
                if args.dataset == 3:
                    d_lr = 0.01
                else:
                    d_lr = 0.03
            else:
                if args.dataset == 3:
                    d_lr = 0.01
                else:
                    d_lr = 0.02
            g_lr = 0.002 / (1. + 10 * p)**0.75
            if t == 0:
                s_im, s_lb = gen_source_batch.__next__()
                _,_, loss_12, loss_t, acc_1, acc_2, acc_t  = sess.run([supervised_op,pred12_optimizer,pred12_loss, predt_loss, task1_acc, task2_acc, taskt_acc],
                                                feed_dict={source_batch:s_im, source_label:s_lb, learning_rate:d_lr, is_training : True, keep_prob : 0.5})
            else:
                s_im, s_lb = gen_source_batch.__next__()
                _, loss_12, acc_1, acc_2 = sess.run([pred12_optimizer, pred12_loss, task1_acc, task2_acc],
                                                feed_dict={source_batch:s_im, source_label:s_lb, learning_rate:d_lr, is_training : True, keep_prob : 0.5})
                t_im, t_lb = gen_labeled_batch.__next__()
                _, loss_t, acc_t = sess.run([supervised_op, predt_loss, taskt_acc],
                                                feed_dict={source_batch:t_im, source_label:t_lb, learning_rate:d_lr, is_training : True, keep_prob : 0.5})
            if idx % 500 == 0:
                print('loss_12 : %.3f loss_t : %.3f acc_1 : %.3f acc_2 : %.3f acc_t : %.3f  (for source and labeled data)' % (loss_12, loss_t, acc_1, acc_2, acc_t))
            if idx % 2000 == 0:
                acc_1, acc_2, acc_t = sess.run([task1_acc, task2_acc, taskt_acc],
                                     feed_dict={source_batch: target_test[:int(target_test.shape[0]/2)], source_label: target_test_label[:int(target_test.shape[0]/2)], is_training: False, keep_prob : 1.0})
                print('acc_1 : %.3f acc_2 : %.3f acc_t : %.3f  (for target data)' % (acc_1, acc_2, acc_t))
        
        # labeling
        cand_batch_num = min(int(target_train.shape[0]/args.batch_size), int(target_train.shape[0] * (t+1)/20 / args.batch_size))
        if t == 0:
            cand_batch_num = min(int(5000/args.batch_size),cand_batch_num)
        else:
            cand_batch_num = min(cand_batch_num, int(20000/args.batch_size))
        print(cand_batch_num)
        labeled_train = np.zeros(target_train.shape,np.float32)
        labeled_train_label = np.zeros(target_train_label.shape,np.float32)
        labeled_num = 0
        correct_labeled_num = 0
        for idx in range(cand_batch_num):
            t_im, t_lb = gen_target_batch.__next__()
            pred1, pred2 = sess.run([pred1_prob,pred2_prob],
                                                feed_dict={source_batch:t_im, source_label:t_lb, is_training : False, keep_prob : 1.0})
            pred1_argmax = np.argmax(pred1,1)
            pred2_argmax = np.argmax(pred2,1)
            pred1_gt     = (pred1[range(args.batch_size),list(pred1_argmax)] > 0.9)
            pred2_gt     = (pred2[range(args.batch_size),list(pred2_argmax)] > 0.9)
            labeled_idx  = np.logical_and((pred1_argmax == pred2_argmax),np.logical_or(pred1_gt,pred2_gt))
            labeled_idx  = list(filter(lambda x : labeled_idx[x], range(args.batch_size)))
            labeled_train[labeled_num:labeled_num + len(labeled_idx)] = t_im[labeled_idx]
            labeled_train_label[range(labeled_num,labeled_num + len(labeled_idx)),pred1_argmax[labeled_idx]] = 1 
            labeled_num  += len(labeled_idx)
            t_lb = np.argmax(t_lb,1)
            correct_labeled_num += np.sum(pred1_argmax[labeled_idx] == t_lb[labeled_idx])
            #if idx == 0:
            #    #print(np.concatenate([np.expand_dims(pred1_argmax,1),np.expand_dims(pred2_argmax,1)],1))
            #    #print(labeled_idx)
            #    #print(np.concatenate([np.expand_dims(pred1[range(args.batch_size),list(pred1_argmax)],1),np.expand_dims(pred2[range(args.batch_size),list(pred2_argmax)],1)],1))
            #    print(len(labeled_idx))
            #    #print(labeled_train_label[labeled_num-len(labeled_idx):labeled_num])
        print('labeled number : %d ' % labeled_num)
        print('labeled acc : %.3f' % ((correct_labeled_num+0.0)/labeled_num))
        acc_1, acc_2, acc_t = sess.run([task1_acc,task2_acc,taskt_acc],
                          feed_dict={source_batch: target_test[int(target_test.shape[0]/2):], source_label: target_test_label[int(target_test.shape[0]/2):], is_training : False, keep_prob : 1.0})
        print('After %dth step acc_1 : %.3f acc_2 : %.3f acc_t : %.3f  (for target data)' % (t, acc_1, acc_2, acc_t)) 

    #source_acc = sess.run(task_acc,
    #                      feed_dict={source_batch: source_test, source_label: source_test_label})
    #test_domain_acc = sess.run(domain_acc,
    #                           feed_dict={source_batch: source_test[:args.batch_size,:,:,:], target_batch: target_test[:args.batch_size,:,:,:]})
    s_emb = sess.run(atda_model.feature, feed_dict={source_batch: source_test[:200,:,:,:], is_training : True, keep_prob : 1.0})
    t_emb = sess.run(atda_model.feature, feed_dict={source_batch: target_test[:200,:,:,:], is_training : True, keep_prob : 1.0})

    #print('Source accuracy : %.4f' % source_acc)
    #print('Target accuracy : %.4f' % target_acc)
    #print('domain accuracy : %.4f' % test_domain_acc)

    tsne= TSNE(perplexity=30, n_components=2, init='pca', n_iter=3000)
    dann_tsne = tsne.fit_transform(np.concatenate([s_emb,t_emb],0))
    emb_lbs = np.concatenate([np.argmax(source_test_label[:200,:],1),np.argmax(target_test_label[:200,:],1)],0)
    emb_domain = np.concatenate([np.zeros(200),np.ones(200)])
    plot_embedding(dann_tsne, emb_lbs, emb_domain, 'Domain Adaptation_from%sto%s' % (source_data,target_data))


