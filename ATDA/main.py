import argparse
from train import train
import tensorflow as tf

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--lamb', type=float, default=0.01)
    parser.add_argument('--batch_size',type=int, default=128)
    parser.add_argument('--dataset',type=int,default=2) ## 0 : mnist->mnistm, 1: mnistm->mnist 2: mnist->svhn 3: svhn->mnist
    args = parser.parse_args()
    if args.dataset > 3:
        raise NotImplementedError('dataset should be less than 3')
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(config=config) as sess:
        train(sess, args)

if __name__ == "__main__":
    main()
